import unittest
import requests
from config import ADMIN_MAIL, ADMIN_PASS, SERVER_BASE_URL


class EntityTestApi(unittest.TestCase):
    def setUp(self):
        self.host = SERVER_BASE_URL

    def test_hello_world(self):
        response = requests.get(self.host)
        self.assertEqual(response.text, "Hello World!")

    def test_login(self):
        response = requests.post(self.host + "/login", json={"data": {"email": ADMIN_MAIL, "password": ADMIN_PASS}})
        json_resp = response.json()
        self.assertEqual(json_resp["success"], True)
        self.assertIsNotNone(json_resp["data"])
        self.assertIsNotNone(json_resp["data"]["token"])
        self.assertTrue("password" not in json_resp["data"])
        self.assertTrue(response.status_code == 200)

    def test_unsuccessful_login(self):
        response = requests.post(self.host + "/login", json={"data": {"email": ADMIN_MAIL, "password": ""}})
        json_resp = response.json()
        self.assertEqual(json_resp["success"], False)
        self.assertIsNone(json_resp["data"])
        self.assertTrue(json_resp["message"] == "Invalid username/ password")
        self.assertTrue(response.status_code == 401)

    def test_register(self):
        response = requests.post(self.host + "/register", json={"data": {"email": "someemail@email.com", "password": "somepass"}})
        json_resp = response.json()
        self.assertEqual(json_resp["success"], True)
        self.assertIsNotNone(json_resp["data"])
        self.assertIsNotNone(json_resp["message"] == "Successful Registration!")
        self.assertIsNotNone(json_resp["data"]["id"])
        self.assertTrue("password" not in json_resp["data"])
        self.assertTrue(response.status_code == 201)

    def test_unsuccessful_register(self):
        response = requests.post(self.host + "/register", json={"data": {"email": ADMIN_MAIL, "password": ADMIN_PASS}})
        json_resp = response.json()
        self.assertEqual(json_resp["success"], False)
        self.assertIsNone(json_resp["data"])
        self.assertTrue("User already exists." in json_resp["message"])
        self.assertTrue(response.status_code == 409)


if __name__ == '__main__':
    unittest.main()
