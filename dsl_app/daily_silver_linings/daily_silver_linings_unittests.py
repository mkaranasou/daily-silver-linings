import json
import os
import unittest
import tempfile
import requests
from daily_silver_linings import app, init_db
from config import ADMIN_MAIL, ADMIN_PASS


class DailySilverLiningsTestCase(unittest.TestCase):
    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        app.testing = True
        self.app = app.test_client()

        with app.app_context():
            init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(app.config['DATABASE'])

    def login(self, email, password):
        data = {"data": {"email": email, "password": password}}
        return self.app.post('/login', data=json.dumps(data))

    def register(self, username, email, password):
        data = {"data": {"username": username, "email": email, "password": password}}
        return self.app.post('/register', data=json.dumps(data))

    def test_valid_register(self):
        resp = self.register("somevaliduser", "someo@mail.com", "pass")
        assert b'Successful Registration!' in resp.data

    def test_invalid_register(self):
        resp = self.register("somevaliduser", ADMIN_MAIL, "1234")
        json_data = json.loads(resp.data)
        assert json_data["success"] is False
        assert "UNIQUE constraint failed" in json_data["message"]

    def test_valid_login(self):
        resp = self.login(ADMIN_MAIL, ADMIN_PASS)
        assert b'token' in rv.data

    def test_invalid_login(self):
        resp = self.login("someother@mail.instead.com", ADMIN_PASS)
        assert b'Invalid username/ password' in resp.data
        resp = self.login(ADMIN_MAIL, 'some pass other than usual')
        assert b'Invalid username/ password' in resp.data


if __name__ == '__main__':
    unittest.main()
