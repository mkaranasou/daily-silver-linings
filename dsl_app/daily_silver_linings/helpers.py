import json
import re
import types
import traceback
from functools import wraps
from flask import url_for, jsonify
from flask_mail import Message
from config import ADMIN_MAIL
from models import CommonMessage


camel_case_to_snake_case = re.compile('((?<=[a-z0-9])[A-Z]|(?!^)[A-Z](?=[a-z]))')


def convert_camel_to_snake(value):
    """
    Convert JavaScript - like camel case to snake case in order to be able
    to keep coding styles intact in api and front-end
    :param value: string, the string to be converted
    :return: string, the camel case string converted to snake case
    """
    return camel_case_to_snake_case.sub(r'_\1', value).lower()


def obj_list_to_dict_list(obj_list):
    """
    Helper to convert a list of objects that have `as_dict` method to a list of dicts
    :param obj_list: list, A list of objects that have "as_dict" method
    :return: list, A list of dictionaries
    """

    return [o.as_dict() for o in obj_list if hasattr(o, "as_dict")]


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if hasattr(o, 'isoformat'):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)


# source: http://flask.pocoo.org/snippets/117/
def list_routes(app):
    import urllib
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
        output.append(line)

    for line in sorted(output):
        print line


def get_module_functions():
    import daily_silver_linings
    # print(dir(daily_silver_linings))
    return [daily_silver_linings.__dict__.get(a) for a in dir(daily_silver_linings)
           if isinstance(daily_silver_linings.__dict__.get(a), types.FunctionType)]


def methods_with_decorators(cls, decorators):
    import daily_silver_linings
    import inspect
    source_lines = inspect.getsourcelines(cls)[0]
    fn_with_decorators = []

    def find_decorators(line, i):
        line = line.strip()
        if "@" in line:
            # split with ( to get the function name
            if line in decorators:
                next_line = source_lines[i + 1]
                if "def " in next_line:
                    name = next_line.split('def')[1].split('(')[0].strip()
                    return name
                else:
                    return find_decorators(next_line, i + 1)

    for i, line in enumerate(source_lines):
        fn_with_decorators.append(find_decorators(line, i))

    return fn_with_decorators


def fortify_route(func):
    """
    Consistent exception handling - for very basic failures
    :param func: Function, the route to be accessed
    :return: Function, the wrapper
    """

    @wraps(func)
    def fortify_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)

        except Exception as e:
            traceback.print_exc()
            resp = jsonify(CommonMessage(message=e.message))
            resp.status_code = 503

            return resp

    return fortify_wrapper


def send_email(to, subject, template):
    from daily_silver_linings import mail

    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=ADMIN_MAIL
    )
    mail.send(msg)

if __name__ == '__main__':
    import daily_silver_linings
    results = list(methods_with_decorators(daily_silver_linings, ['@app.route', '@login_required']))
    print(results)