"""
https://medium.com/me/applications/a8d254bd9b91
"""
from medium import Client

CLIENT_ID = 'mediumclientid'
CLIENT_SECRET = 'mediumclientsecret'

HIGHLIGHTS_URL = 'https://medium.com/@karanasou/highlights?format=json'

# payload/references/quote each {paragraphs[] each text, createdAt}
# get payload/user/createdAt and use it as a param?!
# do not forget: jsonBody = JSON.parse(body.replace('])}while(1);</x>', ''));

HIGHLIGHTS_URL_PAGING = 'https://medium.com/@karanasou/highlights?format=json&limit=100&source="quotes"'


# ------------- Medium Integration ----------------------------------------------------------------------------------- #
# Go to http://medium.com/me/applications to get your application_id and application_secret.
medium_client = Client(application_id=CLIENT_ID, application_secret=CLIENT_SECRET)


# @app.route('/callbacks/medium', methods=['GET', 'POST'])
# def medium():
#     """
#     A successful authentication would return:
#     HTTP/1.1 201 OK
#     Content-Type: application/json; charset=utf-8
#
#     {
#       "token_type": "Bearer",
#       "access_token": {{access_token}},
#       "refresh_token": {{refresh_token}},
#       "scope": {{scope}},
#       "expires_at": {{expires_at}}
#     }
#     :return:
#     """
#
#     if request.args.get("error", False):
#         print("ERROR!!", request.args.get("error", False))
#     else:
#         # Exchange the authorization code for an access token.
#         auth = medium_client.exchange_authorization_code("YOUR_AUTHORIZATION_CODE",
#                                                          MEDIUM_CB)
#
#         # The access token is automatically set on the client for you after
#         # a successful exchange, but if you already have a token, you can set it
#         # directly.
#         medium_client.access_token = auth["access_token"]
#
#         # Get profile details of the user identified by the access token.
#         user = medium_client.get_current_user()
#
#         # Create a draft post.
#         post = medium_client.create_post(user_id=user["id"], title="Title", content="<h2>Title</h2><p>Content</p>",
#                                          content_format="html", publish_status="draft")
#
#         # When your access token expires, use the refresh token to get a new one.
#         medium_client.exchange_refresh_token(auth["refresh_token"])
#
#         # Confirm everything went ok. post["url"] has the location of the created post.
#         print("My new post!", post["url"])
#
#
# @app.route('/medium/authorize', methods=['GET', 'POST'])
# def authorize_medium():
#     # Build the URL where you can send the user to obtain an authorization code.
#     auth_url = medium_client.get_authorization_url("secretstate", MEDIUM_CB, ["basicProfile", "listPublications"])
#     # (Send the user to the authorization URL to obtain an authorization code.)
#     return redirect(auth_url, code=302)
