from datetime import datetime

from enum import Enum
from sqlalchemy import ForeignKey, Column, Integer, String, Boolean, DateTime
from sqlalchemy.orm import relationship, validates
from db import Base


class CommonMessage(object):
    """
    Basic Structure for messages between front-end and back-end,
    in order to envelope errors and give details about what happened
    """
    def __init__(self, success=False, message="", data=None):
        self.success = success
        self.message = message
        self.data = data

    def as_dict(self):
        return {c: getattr(self, c) for c in dir(self) if not c.startswith('__') and not c.startswith("as_dict")}


class AttachmentType(Enum):
    na = -1
    Picture = 0,
    Text = 1


class SerializableMixin(object):
    def as_dict(self, extra_cols=(), remove=()):
        basic_attrs = {c.name: getattr(self, c.name) for c in self.__table__.columns if c not in remove}
        extra_attrs = {}
        if len(extra_cols) > 0:
            for attr in extra_cols:
                d = getattr(self, attr)
                if d is None:
                    continue
                if isinstance(d, list):
                    extra_attrs[attr] = [each.as_dict() for each in d]
                else:
                    extra_attrs[attr] = d.as_dict()
            basic_attrs.update(extra_attrs)

        return basic_attrs


class Attachment(Base, SerializableMixin):
    __tablename__ = "attachment"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    description = Column(String())
    file_path = Column(String())
    file_type = Column(Integer())   # Column(Enum(AttachmentType.na), Enum(AttachmentType))
    silver_lining_id = Column(Integer(), ForeignKey('silver_lining.id'))
    silver_lining = relationship("SilverLining", back_populates="attachments")

    def __repr__(self):
        return '<Attachment %r>' % self.description


class User(Base, SerializableMixin):
    """
    A user can have one or more silver linings and one or more hearts
    (either day hearts or silver lining hearts)
    """
    __tablename__ = "user"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    first_name = Column(String(255))
    last_name = Column(String(255))
    username = Column(String(255), unique=True)
    email = Column(String(255), unique=True)
    password = Column(String(255))
    active = Column(Boolean())
    created_at = Column(DateTime())
    confirmed_at = Column(DateTime())
    is_admin = Column(Boolean(), default=False)
    silver_linings = relationship("SilverLining", passive_deletes=True)
    silver_lining_hearts = relationship('SilverLiningHeart', back_populates="user", cascade="all, delete-orphan", passive_deletes=True)
    day_hearts = relationship('DayHeart', back_populates="user",  cascade="all, delete-orphan", passive_deletes=True)
    _sensitive = ["email", "password", "confirmed_at"]

    def __init__(self,
                 username=None,
                 email=None,
                 password=None,
                 first_name="John",
                 last_name="Doe",
                 active=False,
                 is_admin=False):
        self.username = username
        self.email = email
        self.password = password
        self.active = active
        self.created_at = datetime.utcnow()
        self.first_name = first_name
        self.last_name = last_name
        self.is_admin = is_admin

    def __str__(self):
        return self.email

    def __repr__(self):
        return '<User %r>' % self.email

    @validates('email')
    def validate_email(self, key, email):
        assert '@' in email
        return email


class SilverLining(Base, SerializableMixin):
    """
    A Silver Lining item MUST have a date (OneGoodDay) parent, a user - even if anonymous -
    and may have one or more hearts and one or more attachments.
    """
    __tablename__ = "silver_lining"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    description = Column(String(255))
    create_date = Column(DateTime())
    update_date = Column(DateTime())
    is_approved = Column(Boolean, default=False)
    is_public = Column(Boolean, default=False)
    date_id = Column(Integer(), ForeignKey('one_good_day.id'))
    day = relationship("OneGoodDay", back_populates="silver_linings")
    user_id = Column(Integer(), ForeignKey('user.id'))
    user = relationship("User", back_populates="silver_linings")
    hearts = relationship('SilverLiningHeart', cascade="all, delete-orphan", passive_deletes=True)
    attachments = relationship('Attachment', back_populates='silver_lining', cascade="all, delete-orphan", passive_deletes=True)

    def __init__(self, id=None, description="", create_date=None, is_approved=False, is_public=False, user=None, **kwargs):
        self.description = description
        self.create_date = create_date
        self.is_approved = is_approved
        self.is_public = is_public
        self.user = user
        self.attachments = []

    def __repr__(self):
        return '<SilverLining %r>' % self.description


class OneGoodDay(Base, SerializableMixin):
    """
    A day MAY have one or more SilverLinings,
    and may have one or more hearts
    """
    __tablename__ = "one_good_day"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    date = Column(DateTime)
    is_active = Column(Boolean, default=True)
    is_public = Column(Boolean, default=False)
    silver_linings = relationship('SilverLining', back_populates='day')
    hearts = relationship('DayHeart', back_populates='day')

    def __repr__(self):
        return '<OneGoodDay %r>' % self.date


class DayHeart(Base, SerializableMixin):
    """
    Connecting table between user and buckets - A user may like one or more buckets
    """
    __tablename__ = "day_heart"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    user_id = Column(Integer(), ForeignKey('user.id', ondelete='CASCADE'))
    day_id = Column(Integer(), ForeignKey('one_good_day.id', ondelete='CASCADE'))
    day = relationship("OneGoodDay", back_populates="hearts")
    user = relationship("User", back_populates="day_hearts")

    def __repr__(self):
        return '<DayHeart %r>' % self.id


class SilverLiningHeart(Base, SerializableMixin):
    """
    Connecting table between user and items - A user may like one or more items
    """
    __tablename__ = "silver_lining_heart"
    id = Column(Integer(), primary_key=True, autoincrement=True)
    user_id = Column(Integer(), ForeignKey('user.id', ondelete='CASCADE'))
    item_id = Column(Integer(), ForeignKey('silver_lining.id', ondelete='CASCADE'))
    silver_lining = relationship("SilverLining", back_populates="hearts")
    user = relationship("User", back_populates="silver_lining_hearts")
