from __future__ import print_function
import json
from datetime import datetime
import requests
from flasgger import Swagger
from flask import Flask, request, abort, url_for, session, jsonify
from flask_jwt import JWT
from flask_socketio import SocketIO, emit
from six import wraps
from sqlalchemy import cast, Date
from werkzeug.security import generate_password_hash
from werkzeug.utils import secure_filename, redirect
from flask import send_from_directory
from db import Base, engine, db_session
from flask_cors import CORS
from flask_mail import Mail
from helpers import fortify_route, DateTimeEncoder, obj_list_to_dict_list, convert_camel_to_snake
from token_auth import Auth, SignUp, ForgotPassword, login_required, confirm_token
from config import *
from gevent import monkey

monkey.patch_all()


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'profile' not in session:
            # Redirect to Login page here
            return redirect('/')  # return unauthenticated
        return f(*args, **kwargs)

    return decorated


def identity():
    return Auth().post()


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.
    from models import OneGoodDay, User, SilverLining, SilverLiningHeart, DayHeart, Attachment

    Base.metadata.create_all(bind=engine)

    # add basic and sample users
    admin_user = User(username=ADMIN_USERNAME,
                      email=ADMIN_MAIL,
                      password=generate_password_hash(ADMIN_PASS),
                      active=True,
                      is_admin=True)
    anonymous_user = User(username="anonymous",
                          email=ANONYMOUS_MAIL,
                          active=True)
    me_user = User(username=MY_USERNAME,
                   email=MY_MAIL,
                   password=generate_password_hash(MY_PASS),
                   active=True,
                   is_admin=True)
    db_session.query(User).delete()
    # db_session.query(SilverLiningHeart).delete()
    db_session.add(admin_user)
    db_session.add(anonymous_user)
    db_session.add(me_user)
    db_session.commit()


mail = Mail()
app = Flask(__name__)

app.secret_key = APP_SECRET_KEY
app.config['JWT_SECRET_KEY'] = SECRET_KEY
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['JWT_DEFAULT_REALM'] = JWT_DEFAULT_REALM
app.config['JWT_AUTH_HEADER_PREFIX'] = JWT_AUTH_HEADER_PREFIX
jwt = JWT(app, Auth().post, identity)
app.config["MAIL_SERVER"] = MAIL_SERVER
app.config["MAIL_PORT"] = MAIL_PORT
app.config["MAIL_USE_TSL"] = MAIL_USE_TSL
app.config["MAIL_USE_SSL"] = MAIL_USE_SSL
app.config["MAIL_USERNAME"] = PASSWORD_RESET_EMAIL
app.config["MAIL_DEFAULT_SENDER"] = PASSWORD_RESET_EMAIL
app.config["MAIL_PASSWORD"] = PASSWORD_RESET_PASSWORD

CORS(app)
init_db()
mail.init_app(app)
swagger = Swagger(app)
socket_io = SocketIO(app)

# https://keathmilligan.net/jwt-authentication-with-flask-and-angular-2-a-simple-end-to-end-example/


# https://desouradeep.wordpress.com/2014/12/20/realtiming-with-flask-socketio/
# todo: de-couple this from the api
@socket_io.on("connection-established")
def users_notification(message):
    print("[x] Received:\t", message)
    emit("connection-established", "Hello :)")


@app.route('/')
def hello_world():
    return 'Hello World!'


# Format error response and append status code.
def handle_error(error, status_code):
    resp = jsonify(error)
    resp.status_code = status_code
    return resp


# --------------- Login / Register ----------------------------------------------------------------------------------- #
@app.route('/login', methods=['POST'])
def login():
    return Auth().post()


@app.route('/register', methods=['POST'])
def register():
    return SignUp().post()


@app.route('/forgot-password', methods=['POST'])
def forgot_password():
    return ForgotPassword().post()


@login_required
def confirm_email(token):
    from models import User
    try:
        email = confirm_token(token)
    except:
        pass
        # flash('The confirmation link is invalid or has expired.', 'danger')
    user = User.query.filter_by(email=email).first_or_404()
    if user.confirmed_at:
        pass
        # flash('Already confirmed', 'success')
    else:
        user.confirmed_at = datetime.now()
        db_session.add(user)
        db_session.commit()
        # flash('Thank you!', 'success')
    return redirect(url_for('main.home'))


# --------------- Users ---------------------------------------------------------------------------------------------- #
@app.route('/users/<string:username>', methods=['GET'])
@fortify_route
def get_user(username):
    from models import User
    user = db_session.query(User).filter(User.username == username).first()
    if not user:
        abort(404)
    print("Current user:", user.as_dict(remove=User.sensitive))
    return json.dumps(user.as_dict(remove=User.sensitive), cls=DateTimeEncoder)


@app.route('/users/all', methods=['GET'])
@login_required
def get_users():
    from models import User
    return json.dumps([o.as_dict() for o in db_session.query(User).all()], cls=DateTimeEncoder)


@app.route('/users', methods=['POST', 'PUT'])
@login_required
def update_user():
    from models import User

    if request.method == "POST":
        user = db_session.query(User).filter(User.username == request.form["username"]).first()
        if user:
            # return conflict code
            abort(409, "User {} already exists".format(request.form["username"]))

        user = User()
        user.email = request.form["email"]
        user.username = request.form["username"]
        db_session.add(user)
        db_session.commit()
        print("Current user:", user.as_dict())
        return json.dumps(user.as_dict(), cls=DateTimeEncoder)
    else:
        user = db_session.query(User).filter(User.username == request.form["username"]).first()
        if not user:
            abort(404, "User {} not found".format(request.form["username"]))
        user.first_name = request.form["first_name"]
        user.last_name = request.form["last_name"]
        db_session.commit()


@app.route('/users/<string:username>/silver-linings', methods=['GET'])
@login_required
def get_silver_linings_for_user(username):
    """
    Create / Update a cgd - not sure if this is needed
    :return:
    """
    from models import User
    user = db_session.query(User).filter(User.username == username).first()
    if not user:
        # Bad Request
        abort(404, "User {} not found".format(request.form["username"]))

    user.silver_linings = obj_list_to_dict_list(user.silver_linings)
    resp = {
        "user": user.as_dict(),
    }
    print("Current user:", user.as_dict())
    return json.dumps(resp, cls=DateTimeEncoder), 200


# ------------- Good Days -------------------------------------------------------------------------------------------- #
@app.route('/good-days/<int:year>/<int:month>/<int:day>', methods=['GET'])
def get_current_good_day(year, month, day):
    """
    Gets the current day from the database or creates one
    :return:
    """
    from models import OneGoodDay
    today = datetime(year, month, day, 0, 0)
    current_day = db_session.query(OneGoodDay).filter_by(date=today).first()
    if not current_day:
        current_day = OneGoodDay()
        current_day.date = today
        print("Added the day that was not found", json.dumps(current_day.as_dict(), cls=DateTimeEncoder))
        db_session.add(current_day)
        db_session.commit()
    data = current_day.as_dict()
    data.update({"silver_linings": [o.as_dict(extra_cols=('hearts', 'user'))
                                    for o in current_day.silver_linings
                                    if o.is_public][::-1]})
    print("Current day:", data)
    return json.dumps(data, cls=DateTimeEncoder), 200  # todo: create a wrapper for this


@app.route('/good-days/<int:year>/<int:month>/<int:day>/<int:user_id>', methods=['GET'])
@login_required
def get_current_good_day_for_user(year, month, day, user_id):
    """
    Gets the current day from the database or creates one
    :return:
    """
    from models import OneGoodDay
    today = datetime(year, month, day, 0, 0)
    current_day = db_session.query(OneGoodDay).filter_by(date=today).first()
    if not current_day:
        current_day = OneGoodDay()
        current_day.date = today
        print("Added the day that was not found", json.dumps(current_day.as_dict(), cls=DateTimeEncoder))
        db_session.add(current_day)
        db_session.commit()
    data = current_day.as_dict()
    data.update({"silver_linings": [o.as_dict(extra_cols=('hearts', 'user')) for o in current_day.silver_linings
                                    if o.is_public or (o.user is not None and o.user.id == user_id)][::-1]})
    print("Current day for user :", user_id, data)
    return json.dumps(data, cls=DateTimeEncoder), 200  # todo: create a wrapper for this


@app.route('/good-days/<int:year>/<int:month>/<int:day>', methods=['POST', 'PUT'])
def modify_good_day(year, month, day):
    """
    Create / Update a cgd - not sure if this is needed
    :return:
    """
    from models import OneGoodDay
    today = datetime(year, month, day, 0, 0)
    # today = datetime.utcnow().date()
    if request.method == 'POST':
        current_day = db_session.query(OneGoodDay).filter_by(date=today).first()
        if current_day:
            abort(409, "Day {} already exists - Cannot create".format(today))
        else:
            current_day = OneGoodDay()
            current_day.date = today
            print(json.dumps(current_day.as_dict(), cls=DateTimeEncoder))
            db_session.add(current_day)
            db_session.commit()

        print("Current day created:", current_day.silver_linings)
        return json.dumps(current_day.as_dict(), cls=DateTimeEncoder), 200
    else:
        current_day = db_session.query(OneGoodDay).filter_by(date=today).first()
        if not current_day:
            abort(400, "Day {} was not found - Cannot update.".format(today))
            # cannot find a reason to want to update a day..


# ------------- Silver linings --------------------------------------------------------------------------------------- #
@app.route('/silver-linings/all', methods=['GET'])
def all_silver_linings():
    """
    Get all silver-linings
    :return:list[SilverLining]
    """
    from models import SilverLining

    all_silver_lining_obj = obj_list_to_dict_list(db_session.query(SilverLining).all())
    print("silver_linings:", all_silver_lining_obj)

    return json.dumps(all_silver_lining_obj, cls=DateTimeEncoder)


@app.route('/silver-linings/<int:id>', methods=['GET'])
def silver_lining_by_id(id):
    """
    Get a SilverLining by id
    :return: SilverLining, A jsonified SilverLining
    """
    from models import SilverLining

    silver_lining = db_session.query(SilverLining).filter_by(id=id).first()
    print("silver_linings:", silver_lining)

    if not silver_lining:
        abort(404, "SilverLining with id {} not found.".format(id))

    return json.dumps(silver_lining, cls=DateTimeEncoder), 200


@app.route('/silver-linings/<string:username>', methods=['GET'])
def silver_lining_by_username(username):
    """
    Get all silver-linings for a specific user by username
    :return: list<SilverLining>, a list of jsonified SilverLinings
    """
    from models import SilverLining

    print("username", username)
    silver_lining = db_session.query(SilverLining).filter(SilverLining.user.username == username).first()
    print("silver_linings:", silver_lining)

    return json.dumps(silver_lining, cls=DateTimeEncoder), 200


@app.route('/silver-linings/<int:year>/<int:month>/<int:day>', methods=['GET'])
def silver_linings(year, month, day):
    """
    Get all silver-linings for a specific day
    :return: list<SilverLining>, a list of jsonified SilverLinings
    """
    from models import SilverLining, OneGoodDay

    print("Date:", year, month, day)
    today = datetime(year, month, day, 0, 0)
    current_day = OneGoodDay()
    current_day.date = today
    day_silver_linings = db_session.query(SilverLining).filter(cast(current_day.date, Date) == cast(today, Date),
                                                               is_public=True).all()
    print("silver_linings:", day_silver_linings)
    resp = {"silver_linings": obj_list_to_dict_list(day_silver_linings)}

    return json.dumps(resp, cls=DateTimeEncoder), 200


@app.route('/silver-linings/<int:year>/<int:month>/<int:day>', methods=['POST', 'PUT'])
@login_required
def add_silver_lining(year, month, day):
    """
    Add a SilverLining for a specific date, e.g. 2017/07/01
    :param year: int, the 4 digit year, e.g. 2014
    :param month: int, the up-to-2-digits month, e.g. 4
    :param day: int, the up-to-2-digits day, e.g. 4
    :return: SilverLining, the updated and jsonified SilverLining
    """
    from models import SilverLining, OneGoodDay, User, Attachment
    print(year, month, day, request.method)

    data = json.loads(request.data)
    today = datetime(year, month, day, 0, 0)
    current_day = db_session.query(OneGoodDay).filter_by(date=today).first()

    if not current_day:
        # fixme: either abort or create the current day silently
        abort(404, "Silverlining for {}-{}-{} could not be found.".format(year, month, day))

    if request.method == 'POST':

        if not data or not data["new_silver_lining"]:
            abort(400)

        new_silver_lining = {}

        for k, v in data["new_silver_lining"].items():
            new_silver_lining[convert_camel_to_snake(k)] = v

        user = db_session.query(User).filter(User.id == new_silver_lining["user_id"]).first()
        if not user:
            abort(400, "Could not match SilverLining to user.")
        day = datetime(year, month, day, 0, 0)

        if not current_day:
            current_day = OneGoodDay()
            current_day.date = day

        nsl = SilverLining(user=user, **new_silver_lining)
        nsl.day = current_day
        nsl.create_date = datetime.utcnow().date()
        nsl.is_public = new_silver_lining["is_public"]

        if new_silver_lining["attachment"]:
            for k, v in new_silver_lining["attachment"].items():
                new_silver_lining["attachment"][convert_camel_to_snake(k)] = v
            attachment = Attachment()
            attachment.description = new_silver_lining["attachment"]["description"]
            attachment.file_path = new_silver_lining["attachment"]["file_path"]
            # todo: the following should be calculated here instead of the front end
            attachment.file_type = new_silver_lining["attachment"]["file_type"]
            nsl.attachments.append(attachment)
        db_session.add(nsl)
        db_session.commit()
        print("New Silver Lining:", nsl.as_dict())
        return json.dumps(nsl.as_dict(), cls=DateTimeEncoder), 200
    else:
        # todo: else does not make much sense, we need to be specific about which silverlining to modify - remove this

        for k, v in data["silver_lining"].items():
            data["silver_lining"][convert_camel_to_snake(k)] = v

        silver_lining = db_session.query(SilverLining).filter_by(day=current_day,
                                                                 id=data["silver_lining"]["id"]).first()
        if not silver_lining:
            abort(404)
        else:
            silver_lining.description = data["silver_lining"]["description"]
            silver_lining.update_date = datetime.utcnow().date()
            silver_lining.is_public = data["silver_lining"]["is_public"]
            db_session.commit()
            return json.dumps(silver_lining.as_dict(), cls=DateTimeEncoder)


@app.route('/silver-linings/<id>/delete', methods=['POST', 'DELETE'])
@login_required
def delete_silver_lining(id):
    from models import SilverLining

    silver_lining = SilverLining.query.filter_by(id=id)
    if not silver_lining:
        abort(404, "Cannot find Silverlining with id {} to delete.".format(id))
    deleted = silver_lining.delete()

    if deleted:
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        abort(500, "Could not delete Silver Lining {}".format(id))


# ------------- Silver lining Hearts --------------------------------------------------------------------------------- #
@app.route('/silver-lining-hearts/hearts-for/<int:silverlining_id>', methods=['GET'])
def get_a_silver_linings_hearts(silverlining_id):
    from models import SilverLiningHeart
    silver_lining_hearts = db_session.query(SilverLiningHeart).filter_by(item_id=silverlining_id).all()
    resp = {
        "hearts_by": [o.user.as_dict() for o in silver_lining_hearts]
    }
    return json.dumps(resp, cls=DateTimeEncoder), 200


@app.route('/silver-lining-hearts/hearted-by/<int:user_id>', methods=['GET'])
@login_required
def get_a_users_silver_lining_hearts(user_id):
    from models import SilverLiningHeart
    user_silver_lining_hearts = db_session.query(SilverLiningHeart).filter_by(user_id=user_id).all()
    resp = {
        "hearted": [o.silver_lining.as_dict() for o in user_silver_lining_hearts]
    }
    return json.dumps(resp, cls=DateTimeEncoder)


@app.route('/silver-lining-hearts/<int:silverlining_id>/<int:user_id>', methods=['POST', 'PUT'])
@login_required
def add_silver_lining_heart(silverlining_id, user_id):
    print(silverlining_id, user_id, request.method)
    if request.method == 'POST':
        from models import SilverLining, User, SilverLiningHeart
        silver_lining = db_session.query(SilverLining).filter_by(id=silverlining_id).first()
        if silver_lining:
            user = db_session.query(User).filter_by(id=user_id).first()
            if user:
                heart = SilverLiningHeart()
                heart.user = user
                heart.silver_lining = silver_lining
                db_session.add(heart)
                db_session.commit()
                print("New Silver Lining <3:", heart.as_dict())
                return json.dumps(heart.as_dict(), cls=DateTimeEncoder)
    return abort(404)


@app.route('/silver-lining-hearts/<int:silverlining_id>/<int:user_id>/delete', methods=['POST', 'PUT'])
@login_required
def delete_silver_lining_heart(silverlining_id, user_id):
    print(silverlining_id, user_id, request.method)
    if request.method == 'POST':
        from models import SilverLining, User, SilverLiningHeart
        heart_deleted = db_session.query(SilverLiningHeart).filter_by(item_id=silverlining_id, user_id=user_id).delete()
        if heart_deleted > 0:
            print("Silver Lining <3 Deleted:", SilverLiningHeart)
            return json.dumps(heart_deleted, cls=DateTimeEncoder)
    return abort(404)


# ------------- Day Hearts ------------------------------------------------------------------------------------------- #
@app.route('/day-hearts/hearts-for/<int:day_id>', methods=['GET'])
def get_a_day_hearts(day_id):
    from models import DayHeart
    day_hearts = db_session.query(DayHeart).filter_by(day_id=day_id).all()

    resp = {
        "hearts_by": [o.user.as_dict() for o in day_hearts]
    }

    return json.dumps(resp, cls=DateTimeEncoder)


@app.route('/day-hearts/hearts-for/<int:year>/<int:month>/<int:day>', methods=['GET'])
def get_day_hearts_by_date(year, month, day):
    from models import DayHeart, OneGoodDay
    today = datetime(year, month, day, 0, 0)
    day = db_session.query(OneGoodDay).filter_by(date=today)
    if not day:
        return abort(404, "Day for date {}-{}-{} not found!".format(year, month, day))
    day_hearts = db_session.query(DayHeart).filter_by(day=day).all()

    resp = {
        "hearts_by": [o.user.as_dict() for o in day_hearts]
    }

    return json.dumps(resp, cls=DateTimeEncoder)


@app.route('/day-hearts/hearted-by/<int:user_id>', methods=['GET'])
@login_required
def get_a_users_day_hearts(user_id):
    from models import DayHeart
    user_day_hearts = db_session.query(DayHeart).filter_by(user_id=user_id).all()
    resp = {
        "hearted": [o.day.as_dict() for o in user_day_hearts]
    }
    return json.dumps(resp, cls=DateTimeEncoder)


@app.route('/day-hearts/<int:day_id>/<int:user_id>', methods=['POST', 'PUT'])
@login_required
def add_day_heart(day_id, user_id):
    print(day_id, user_id, request.method)
    if request.method == 'POST':
        from models import DayHeart, User
        day = db_session.query(DayHeart).filter_by(id=day_id).first()
        if day:
            user = db_session.query(User).filter_by(id=user_id).first()
            if user:
                heart = DayHeart()
                heart.user = user
                heart.day = day
                db_session.add(heart)
                db_session.commit()
                print("New Day <3:", heart.as_dict())
                return json.dumps(heart.as_dict(), cls=DateTimeEncoder), 201
    return abort(404)


@app.route('/day-hearts/<int:day_id>/<int:user_id>/delete', methods=['POST', 'PUT'])
@login_required
def delete_day_heart(day_id, user_id):
    if request.method == 'POST':
        from models import User, DayHeart
        heart_deleted = db_session.query(DayHeart).filter_by(day_id=day_id, user_id=user_id).delete()
        if heart_deleted > 0:
            print("Day <3 Deleted:", DayHeart)
            return json.dumps(heart_deleted, cls=DateTimeEncoder)
    return abort(404)


# ------------- Callbacks -------------------------------------------------------------------------------------------- #
@app.route('/callbacks/auth0', methods=['GET', 'POST'])
def auth0_callback():
    """
    :return:
    """

    code = request.args.get('code')

    json_header = {'content-type': 'application/json'}

    token_url = "https://{domain}/oauth/token".format(domain='digitalsense.eu.auth0.com')

    token_payload = {
        'client_id': '9vy6f70FeulVVW1cOfhoNxirl79TTj28',
        'client_secret': 'dp_OGjrxNdWkOEgd3vXbxeCUdytg8N0J_UJOQdDBYF5gh6GooD1jU82o3HrNnxGr',
        'redirect_uri': 'http://localhost:5000/callbacks/auth0',
        'code': code,
        'grant_type': 'authorization_code'
    }

    token_info = requests.post(token_url, data=json.dumps(token_payload), headers=json_header).json()

    user_url = "https://{domain}/userinfo?access_token={access_token}" \
        .format(domain='digitalsense.eu.auth0.com', access_token=token_info['access_token'])

    user_info = requests.get(user_url).json()

    # We're saving all user information into the session
    session['profile'] = user_info

    # Redirect to the User logged in page that you want here
    # In our case it's /dashboard
    # return redirect(CLIENT_BASE_URL + "?"+ json.dumps(user_info))
    return json.dumps(user_info)


# ------------- Uploads ---------------------------------------------------------------------------------------------- #
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# http://flask.pocoo.org/docs/0.12/patterns/fileuploads/
@app.route('/uploads', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'files' not in request.files:
        return 500
    f = request.files['files']
    # if user does not select file, browser also
    # submit a empty part without filename
    if f.filename == '':
        return 500
    if f and allowed_file(f.filename):
        filename = secure_filename(f.filename)
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return json.dumps(url_for('uploaded_file', filename=filename))


@app.route('/uploads/<filename>', methods=['GET'])
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


if __name__ == '__main__':
    # app.run(debug=True)
    socket_io.run(app, debug=True)