# based on https://github.com/Leo-G/Flask-Scaffold/blob/master/app/baseviews.py
import json
import jwt
from jwt import DecodeError, ExpiredSignature
from werkzeug.routing import ValidationError
from config import SECRET_KEY, PASSWORD_RESET_EMAIL, SECURITY_PASSWORD_SALT
from datetime import datetime, timedelta
from functools import wraps
from flask import g, jsonify, request, abort
from db import db_session
from models import User, CommonMessage
from werkzeug.security import check_password_hash, generate_password_hash
from sqlalchemy.exc import SQLAlchemyError
from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer

# login1 = Blueprint('login', __name__)


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(SECRET_KEY)
    return serializer.dumps(email, salt=SECURITY_PASSWORD_SALT)


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(SECRET_KEY)
    try:
        email = serializer.loads(
            token,
            salt=SECURITY_PASSWORD_SALT,
            max_age=expiration
        )
    except:
        return False
    return email


def create_token(user):
    """

    :param user:
    :return:
    """
    payload = {
        'sub': user.id,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=1),
        'scope': user.is_admin
    }
    token = jwt.encode(payload, SECRET_KEY)
    return token.decode('unicode_escape')


def parse_token(req):
    """

    :param req:
    :return:
    """
    token = req.headers.get('Authorization').split()[1]
    return jwt.decode(token, SECRET_KEY, algorithms='HS256')


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.headers.get('Authorization'):
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response

        try:
            payload = parse_token(request)
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response

        g.user_id = payload['sub']

        return f(*args, **kwargs)

    return decorated_function


def admin_login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.headers.get('Authorization'):
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response

        try:
            # print(request.headers.get('Authorization'))
            payload = parse_token(request)
            if payload['scope'] != "admin":
                response = jsonify(error='Admin Access Required')
                response.status_code = 401
                return response
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response

        g.user_id = payload['sub']

        return f(*args, **kwargs)

    return decorated_function


class Auth(object):

    def post(self):
        msg = CommonMessage()
        raw_dict = request.get_json(force=True)
        data = raw_dict['data']
        email = data.get('email', '')
        password = data.get('password', '')
        user = User.query.filter_by(email=email).first()
        if user is None:
            msg.message = "Invalid username/ password"
            return json.dumps(msg.as_dict()), 401
        if check_password_hash(user.password, password):
            token = create_token(user)
            msg.success = True
            msg.data = {
                        'token': token,
                        'id': user.id,
                        'username': user.username,
                        'email': user.email
            }
            return json.dumps(msg.as_dict()), 200
        else:
            msg.message = "Invalid username/ password"
            return json.dumps(msg.as_dict()), 401


class SignUp(object):

    def post(self):
        msg = CommonMessage()
        raw_dict = request.get_json(force=True)
        try:
            request_dict = raw_dict['data']
            user = User(email=request_dict.get('email', ''),
                        password=generate_password_hash(request_dict.get('password', '')),
                        username=request_dict.get('username', ''),
                        active=False,
                        is_admin=False)
            db_session.add(user)
            db_session.commit()

            registered_user = User.query.get(user.id)
            if not registered_user:
                msg.message = "Oh-oh! Something went very wrong! Contact admin please!"
                return jsonify(msg.as_dict()), 404

            msg.data = {
                "id": registered_user.id,
                "email": registered_user.email,
                "username": registered_user.username
            }
            msg.message = "Successful Registration!"
            msg.success = True
            return jsonify(msg.as_dict()), 201

        # except ValidationError as err:
        #     msg.message = err.messages
        #     return jsonify(msg.as_dict()), 403

        except SQLAlchemyError as e:
            db_session.rollback()
            #todo: needs better handling regarding error msgs
            if "Integrity error" in e:
                msg.message = "User already exists."

            return jsonify(msg.as_dict()), 409


class ForgotPassword(object):

    def patch(self):
        if not request.headers.get('Authorization'):
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response

        try:
            print(request.headers.get('Authorization'))
            payload = parse_token(request)
            user_id = payload['sub']
            user = User.query.get_or_404(user_id)
            print(request.data)
            raw_dict = request.get_json(force=True)
            request_dict = raw_dict['data']

            user.password = generate_password_hash(request_dict['password'])
            try:
                user.update()
                return 201

            except SQLAlchemyError as e:
                db_session.rollback()
                resp = jsonify({"error": str(e)})
                resp.status_code = 401
                return resp
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response

    def post(self):
        from daily_silver_linings import mail

        msg = CommonMessage()
        request_dict = request.get_json(force=True)['data']
        email = request_dict.get('email', '')
        user = User.query.filter_by(email=email).first()
        if user is None:
            msg.message = "Your email is not in our database, are you sure it is correct?"
            return json.dumps(msg.as_dict()), 404
        if user is not None:
            token = create_token(user)

            try:
                # Prepare and send email
                email_msg = Message("Here's your Password Reset Link :)", recipients=[email], bcc=[PASSWORD_RESET_EMAIL])
                email_msg.html = "Unfortunately this is under construction! " \
                                 "We will be back and fully operational soon! " \
                                 "In the mean time, drop us a word if you need help:" +\
                                 PASSWORD_RESET_EMAIL.format(token=token)
                                # TODO: send a url to this server to reset password
                mail.send(email_msg)

                msg.success = True
                msg.message = "Reset link sent successfully. Please, check your email!"
                return jsonify(msg.as_dict()), 201

            except RuntimeError as e:
                msg.message = str(e)
                return jsonify(msg.as_dict()), 500