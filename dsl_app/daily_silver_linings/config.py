import os

APP_SECRET_KEY = "so secret"
SECRET_KEY = "so secret"
PASSWORD_RESET_EMAIL = "my.daily.silverlinings@gmail.com"
PASSWORD_RESET_PASSWORD = "some reset pass"
MAIL_PORT = 465     # 587 for ssl
MAIL_USE_SSL = True
MAIL_USE_TSL = False
MAIL_SERVER = "smtp.gmail.com"

JWT_AUTH_HEADER_PREFIX = 'Bearer'
JWT_DEFAULT_REALM = 'Login Required'

ADMIN_USERNAME = "admin"
ADMIN_MAIL = "my.daily.silverlinings@gmail.com"
ADMIN_PASS = "somepassword"
ANONYMOUS_MAIL = "anonymous@email.com"
MY_USERNAME = "Maria"
MY_MAIL = "myemail@email.com"
MY_PASS = "somepassword"
SERVER_BASE_URL = 'http://localhost:5000'
CLIENT_BASE_URL = 'http://localhost:4200'
UPLOAD_FOLDER = '.data/uploads'
PRODUCTION_UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/data/uploads'

MEDIUM_CLIENT_ID = ''
MEDIUM_CLIENT_SECRET = ''
MEDIUM_REDIRECT_URI = ''
MEDIUM_GRANT_TYPE = ''
MEDIUM_DOMAIN = ''
MEDIUM_CB = ''

SECURITY_PASSWORD_SALT = 'salt'

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
