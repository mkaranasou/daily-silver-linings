from sqlalchemy import create_engine, Column, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine('sqlite:///test.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


# To add a column
def add_column(engine, table_name, column):
    """
    Alters a table to add a column in order to avoid the fuss when *small* changes in models.
    This has been tested with SQLite only.
    :param engine: engine: SQLAlchemy engine instance
    :param table_name: str: the name of the table
    :param column: Column: the column instance to add
    :return: None
    """
    column_name = column.compile(dialect=engine.dialect)
    column_type = column.type.compile(engine.dialect)
    engine.execute('ALTER TABLE %s ADD COLUMN %s %s' % (table_name, column_name, column_type))

if __name__ == '__main__':
    from sqlalchemy import create_engine, Column, Boolean
    column = Column('is_admin', Boolean())
    add_column(engine, 'user', column)
