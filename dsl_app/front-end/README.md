# FrontEnd

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.25.5.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to GitHub Pages

Run `ng github-pages:deploy` to deploy to GitHub Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


API
---
Good days:
- Get a good day by date `'/good-days/<int:year>/<int:month>/<int:day>'`
- Update a good day by date `'/good-days/<int:year>/<int:month>/<int:day>'`

Users:
- Get a user by username `'/users/<string:username>'`
- Get all users `'/users/all'`
- Create/ Update a user `'/users'`
- Get SilverLinings for User `/users/<string:username>/silver-linings'`

Silver Linings:
- Get all SilverLinings `'/silver-linings/all'`
- Get a specific SilverLining by id `'/silver-linings/<int:id>'`
- Get SilverLinings of a specific user by username `'/silver-linings/<string:username>'`
- Get SilverLinings by date `'/silver-linings/<int:year>/<int:month>/<int:day>'`
- Create/ Update a SilverLining by date `'/silver-linings/<int:year>/<int:month>/<int:day>'`
- Delete a SilverLining by id `'/silver-linings/<id>/delete'`

Silver Lining Hearts:
- Get SilverLiningHearts for a specific SilverLining by SilverLining id `'/silver-lining-hearts/hearts-for/<int:silverlining_id>'`
- Get the SilverLiningHearts for a user by user id (returns SilverLinings also) `'/silver-lining-hearts/hearted-by/<int:user_id>'`
- Create Update a SilverLiningHeart by SilverLining id and User id `'/silver-lining-hearts/<int:silverlining_id>/<int:user_id>'`
- Delete a SilverLiningHeart by SilverLining id and User id `'/silver-lining-hearts/<int:silverlining_id>/<int:user_id>/delete'`

Day Hearts:
- Get DayHearts for a specific GoodDay by Day id `'/day-hearts/hearts-for/<int:day_id>'`
- Get DayHearts for a specific GoodDay by date `'/day-hearts/hearts-for/<int:year>/<int:month>/<int:day>'`
- Get the DayHearts for a user by user id (returns GoodDays also) `'/day-hearts/hearted-by/<int:user_id>'`
- Create Update a DayHearts by GoodDay id and User id `'/day-hearts/<int:day_id>/<int:user_id>'`
- Delete a DayHearts by GoodDay id and User id `'/day-hearts/<int:day_id>/<int:user_id>/delete'`

Uploads:
- Upload a file `'/uploads'`
- View an uploaded file `'/uploads/<filename>'`

