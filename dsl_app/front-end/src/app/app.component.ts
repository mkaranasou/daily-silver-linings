import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {Http} from "@angular/http";
//import {Auth} from './services/auth.service'

@Component({
  selector: 'app',
  templateUrl: `./templates/main.html`,
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewInit {
  http: Http;
  title: string;

  constructor(@Inject(Http) http){
    this.http = http;
    this.title = "Daily Silver Linings";
  }

  ngAfterViewInit(): void {

    // source: https://jmperezperez.com/medium-image-progressive-loading-placeholder/
    let placeholder = <HTMLElement>document.querySelector('.placeholder');
    let small = <HTMLImageElement>placeholder.querySelector('.calm');
    // 1: load small image and show it
    let img = new Image();
    img.src = small.src;
    img.onload = function () {
      small.classList.add('loaded');
    };

    // 2: load large image
    let imgLarge = new Image();
    imgLarge.src = (<any>placeholder).dataset.large;
    imgLarge.onload = function () {
      imgLarge.classList.add('loaded');
    };
    placeholder.appendChild(imgLarge);
    placeholder.removeChild(small);
    imgLarge.className="calm loaded";

  }



}
