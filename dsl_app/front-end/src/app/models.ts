import {Helpers} from "./helpers";
import {serviceConfig} from "./services/service.config";
export enum ActionType{
  none,
  itemComplete,
  itemAdded,
  itemRemoved,
  itemFavorited,
  dayAdded,
  dayRemoved,
  dayUpdated,
  dayFavorited,
  login,
  logout,
  register,
  resetPassword
}

export class Action{
  id:string;
  type:ActionType;
  actor: User;
  item:any;
  message: string;

  constructor(options?){
    options = options || {};
    this.id = options.id || null;
    this.type = options.type || ActionType.none;
    this.actor = options.actor || null;
    this.item = options.item || null;
    this.message = this.getMessage();
  }

  getMessage():string{

    switch (this.type){ // todo: make this smarter..
      case ActionType.none:
        return "";
      case ActionType.itemComplete:
        return this.actor.username + " completed " + this.item.description + "!!!";
      case ActionType.itemAdded:
        return this.actor.username + " added '" + this.item.description + "'!";
      case ActionType.itemRemoved:
        return this.actor.username + " removed '" + this.item.description + "'.";
      case ActionType.itemFavorited:
        return this.actor.username + " liked '" + this.item.description + "'!";
      case ActionType.dayAdded:
        return this.actor.username + " added " + this.item.dateStr + " day!";
      case ActionType.dayRemoved:
        return this.actor.username + " removed " + this.item.dateStr + " day.";
      case ActionType.dayUpdated:
        return this.actor.username + " updated " + this.item.dateStr + " day!!!";
      case ActionType.dayFavorited:
        return this.actor.username + " liked " + this.item.dateStr + " day!";
      default:
        return "";
    }
  }
}

export class Attachment{
  description: string;
  filePath: string;
  fileType: string;
  parent: SilverLining;

  constructor(options?) {
    options = options || {};
    this.description = options.name || "";
    this.filePath = options.filePath || "";
    let temp = this.filePath.split(".");
    this.fileType = options.fileType || temp[temp.length-1];
    this.parent = options.parent;
  }
  asJSON() {
    return {
      "description": this.description,
      "filePath": this.filePath,
      "fileType": this.fileType,
    }
  }

  toEditor(){
    switch (this.fileType){
      case "tiff":
      case "jpeg":
      case "png":
      case "jpg":
        return "<img src='" + this.filePath + "' class='card-content'/>";
      case "txt":
      case "pdf":
        return "<a href='" + this.filePath + "'>" + this.description + "</a>";
    }
  }
}

export class SilverLiningHeart{
  id: number;
  userId:number;
  itemId:number;

  constructor(options?){
    options = options || {};
    options = Helpers.convertToCamelCase(options);
    this.id = options.id || null;
    this.itemId = options.itemId || null;
    this.userId =  options.userId || null;
  }
}

export class SilverLining{
  id:number;
  description:string;
  isApproved: boolean;
  isPublic: boolean;
  allowedToSee: boolean;
  attachment: Attachment;
  hearts: SilverLiningHeart[];
  user:User;

  constructor(options?){
    options = options || {};
    options = Helpers.convertToCamelCase(options);
    this.id = options.id || null;
    this.description = options.description;
    this.user = options.user || new User({username:"anonymous"});
    this.isApproved = options.isApproved === true;
    this.isPublic = options.isPublic === true;
    this.allowedToSee = options.allowedToSee === true;
    this.hearts = options.hearts? this.loadHearts(options.hearts) : [];
    this.attachment = this.loadAttachment(options.attachment);

    this.description = this.toEditor();
  }

  asJSON(){
    return {
      "id": this.id,
      "description": this.description,
      "isApproved": this.isApproved,
      "isPublic": this.isPublic,
      "userId": this.user.id,
      "attachment": this.attachment? this.attachment.asJSON() :  null
    }
  }

  toEditor(){
    return this.attachment? this.attachment.toEditor() : this.description;
  }

  loadAttachment(attachment){
    if(!attachment) return null;
    return new Attachment(attachment);
  }

  loadHearts(hearts){
    let hs = [];
    for(let i=0; i < hearts.length; i++){
      let heart = Helpers.convertToCamelCase(hearts[i]);
      hs.push(new SilverLiningHeart(heart));
    }
    return hs;
  }
}

export class OneGoodDay{
  id:number;
  date:Date;
  dateStr: string;
  silverLinings:SilverLining[];

  constructor(options?){
    options = options || {};
    options = Helpers.convertToCamelCase(options);
    this.id = options.id || null;
    // if we have a date then it is from the server - thus in utc - convert it back to local
    this.date = options.date? OneGoodDay.getLocalDate(options.date) : new Date();
    this.dateStr = options.dateStr || OneGoodDay.getStrDate(this.date);
    this.silverLinings = options.silverLinings? this.loadSilverLinings(options.silverLinings) : [];
  }

  public static getLocalDate(date){
    date = new Date(date);
    date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return date;
  }

  public static getStrUTCToLocalDate(date?){
    date = new Date(date);
    date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();

  }

  public static getStrUTCDate(date?){
    date = date || new Date();
    return date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();
  }

  public static getStrDate(date?){
    date = date || new Date();
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
  }

  public loadSilverLinings(silverLinings){
    let sls = [];
    for(let i=0; i < silverLinings.length; i++){
      let sl = Helpers.convertToCamelCase(silverLinings[i]);
      sls.push(new SilverLining(sl));
    }
    return sls;
  }
}

export class User{
  id:number;
  username:string;
  email:string;
  password:string;
  profile:any;
  isValid:boolean;
  updateOthers: boolean;
  silverLinings:SilverLining[];

  constructor(options?){
    options = options || {};
    options = Helpers.convertToCamelCase(options);

    this.id = options.id;
    this.username = options.username;
    this.email = options.email;
    this.password = options.password;
    this.profile = options.profile || {};
    this.isValid = options.is_valid === true;
    this.updateOthers = options.updateOthers === true;
    this.silverLinings =  options.silverLinings || [];
  }

  isAnonymous(){
    return this.username == "anonymous" && this.password == null;
  }

}

export class Dashboard{
  currentDate: OneGoodDay;
  user: User;
  actions: Action[];

  constructor(options?){
    options = options || {};
    this.currentDate = options.currentDate || new OneGoodDay();
    this.user = options.user || new User({name:"Anonymous", isValid: true});
  }
}

export class CommonMessage{
  success: boolean;
  message: string;
  data: any;
  action: ActionType;

  constructor(options?){
    options = options || {};

    this.success = options.success || false;
    this.message = options.message || "";
    this.data = options.data || null;
    this.action = options.action || ActionType.none;
  }
}

export class BaseSvc{
    inProgress: boolean;
    baseUrl: string;
    host = serviceConfig.actionUrl;

    setInprogress(){
        this.inProgress = true;
    }

    done(){
      this.inProgress = false;
    }

    isInProgress(){
      return this.inProgress;
    }
}

