import {Component, OnInit, Inject, Injectable, AfterViewInit, OnDestroy} from '@angular/core';
import {Http, HttpModule, Headers, RequestOptions} from '@angular/http';
import {
  Dashboard, User, Action, ActionType, OneGoodDay, SilverLining, SilverLiningHeart,
  CommonMessage, Attachment
} from '../models';
import {DatePickerOptions, DateModel} from 'ng2-datepicker';
import {Auth} from '../services/auth.service'
import {Router, ActivatedRoute, Params} from '@angular/router';
import 'rxjs/Rx';
import {Subscription} from "rxjs";
import {InProgressSvc} from "../services/inProgressSvc.service";
import {DaySvc} from "../services/day.service";
import {UserSvc} from "../services/user.service";

declare const jQuery;
declare const $;
declare const tinymce;
declare const tinyMCE;
declare const HOST;
declare const MediumEditor;
declare const localStorage;

@Component({
  selector: 'main-container',
  templateUrl: '../templates/day-stream.component.html',
  styleUrls: ["../css/day-stream.component.css"],
  providers: [HttpModule]
})
@Injectable()
export class DayStreamComponent implements OnInit, OnDestroy, AfterViewInit {
  dashboard: Dashboard;
  actions: Action[];
  currentDate: string;
  chosenDate: Date;
  newSilverLining: SilverLining;
  http: any;
  editorOpen: boolean;
  inEditMode: boolean;
  date: DateModel;
  options: DatePickerOptions;
  pickADate: boolean;
  uploadAFile: boolean;
  inProgress: boolean;
  detailsOpen: boolean;
  fileUpload: File;
  isAFileSl : boolean;
  datePickerOptions: any;
  activeSilverLining: SilverLining;
  mediumEditor: any;
  authSubscription: Subscription;
  commonMsg: CommonMessage;


  constructor(@Inject(Http) Http,
              private auth: Auth,
              private inProgressSvc: InProgressSvc,
              private daySvc: DaySvc,
              private userSvc: UserSvc,
              private activatedRoute: ActivatedRoute
  ) {
    this.http = Http;
    this.currentDate = OneGoodDay.getStrDate(new Date());
    this.chosenDate = new Date();
    this.dashboard = new Dashboard();
    this.dashboard.user = null;
    this.newSilverLining = new SilverLining({isPublic: true});
    this.editorOpen = false;
    this.inEditMode = false;
    this.options = new DatePickerOptions();
    this.pickADate = false;
    this.uploadAFile = false;
    this.inProgress = this.inProgressSvc.isInProgress();
    this.detailsOpen = false;
    this.fileUpload = null;
    this.actions = [];
    this.activeSilverLining = null;
    this.mediumEditor = null;
    // due to issue of date - day not displaying correctly:
    // https://github.com/jkuri/ng2-datepicker/issues/111
    this.datePickerOptions = {
      maxDate: new Date(),
      firstWeekdaySunday: true
    };
  }

  processMsg(msg: CommonMessage) {
    switch (msg.action) {
      case ActionType.logout:
      case ActionType.login:
        this.afterAuth(msg);
        break;
      case ActionType.register:

        break;
      case ActionType.resetPassword:

        break;
      default:
        console.warn("I got something I wasn't supposed to...", msg)
    }
  }

  afterAuth(msg: CommonMessage) {
    if (msg.success) {
      this.getCurrentUser();
      this.getDate();
      this.initNewSl();
    }
  }

  onCloseDetails(close:boolean){
    this.detailsOpen = false;
  }

  ngOnInit(): void {
     // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
        console.log("params", params, params["year"]);
      });

    let self = this;
    this.inProgressSvc.setInprogress();
    this.authSubscription = this.auth.getMessage().subscribe(commonMsg => {
      this.commonMsg = commonMsg;
      this.processMsg(commonMsg);
    });
    this.getCurrentUser().then(function () {

      self.getDate().then(function () {
        self.initNewSl();
        self.inProgressSvc.done();
      });
    });
  }

  ngAfterViewInit(): void {
    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

  }

  getCurrentUser() {
    let self = this;
    this.dashboard.user = null;
    return new Promise(function (resolve, reject) {
      self.dashboard.user = self.userSvc.getCurrentUser();
      resolve();
    });
  }

  getDate(date?: Date) {
    let self = this;
    return this.daySvc.getDate(this.dashboard.user, date).then(function (ogd) {
          self.dashboard.currentDate = new OneGoodDay(ogd);
          self.storeCurrentDate(self.dashboard.currentDate.dateStr);
    });
  }

  getStoredDate() {
    return localStorage.getItem("current_view_date");
  }

  storeCurrentDate(date: string) {
    return localStorage.setItem("current_view_date", date);
  }

  addSilverLining(fromEditor?: boolean) {
    if(this.userSvc.user.isAnonymous()){
      alert("Please login/register to post :) ");
      return;
    }
    if (fromEditor) {
      if (this.inEditMode) {
        this.newSilverLining = this.activeSilverLining;
      }
      this.newSilverLining.description = tinyMCE.activeEditor.getContent();
    }
    if (this.newSilverLining.description == null || this.newSilverLining.description.length < 2) {
      this.newSilverLining.description = "";
      alert("Please enter something here :) ");
      return;
    }
    this.inProgress = true;
    this.dashboard.currentDate.silverLinings.unshift(
      new SilverLining({
        user: this.dashboard.user,
        description: this.newSilverLining.description,
        isApproved: false,
        isPublic: this.newSilverLining.isPublic || this.dashboard.user.username == "anonymous",
        attachment: this.newSilverLining.attachment
      })
    );

    this.activeSilverLining = this.dashboard.currentDate.silverLinings[0];
    this.newSilverLining.description = "";

    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    this.http.post(HOST + "/silver-linings/" +
      this.dashboard.currentDate.date.getUTCFullYear() + "/" +
      (this.dashboard.currentDate.date.getUTCMonth() + 1) + "/" +
      this.dashboard.currentDate.date.getUTCDate(),
      {"new_silver_lining": this.dashboard.currentDate.silverLinings[0].asJSON()},
      {headers: headers}
    )
      .map(res => res.json())
      .subscribe(data => {
        if (data) {
          this.dashboard.currentDate.silverLinings[0].id = data["id"];
          this.inProgress = false;
          this.dashboard.currentDate.silverLinings[0].description = this.dashboard.currentDate.silverLinings[0].toEditor();
        }
      });
  }

  editSilverLining(sl: SilverLining) {
    this.hideAllTooltips()
    this.inEditMode = true;
    this.activeSilverLining = sl;
    this.editorOpen = !this.editorOpen;
    if (!tinyMCE.activeEditor) {
      this.initEditor()
    }
    tinyMCE.activeEditor.setContent(sl.description);
  }

  deleteSilverLining(sl: SilverLining) {
    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    sl.isApproved = false;
    this.http.post(HOST + "/silver-linings/" + sl.id + "/delete", {}, options)
      .map(res => res.json())
      .subscribe(data => {
        let index = this.dashboard.currentDate.silverLinings.indexOf(sl);
        this.dashboard.currentDate.silverLinings.splice(index, 1);
      });

  }

  goToPreviousDay() {
    let d = new Date(this.dashboard.currentDate.date.getTime());
    this.getDate(new Date(d.setDate(d.getDate() - 1)))
  }

  goToNextDay() {
    let d = new Date(this.dashboard.currentDate.date.getTime());
    this.getDate(new Date(d.setDate(d.getDate() + 1)))
  }

  applyDate() {
    if (!this.date) {
      let inputDate = (<any>document.getElementsByClassName("datepicker-input")[0]);
      this.date = new DateModel();
      console.log(inputDate, inputDate.value)
      if (inputDate.value != undefined) {
        // inputDate = inputDate.value.split(/-/g);
        // this.date.day = inputDate[2];
        // this.date.month = inputDate[1];
        // this.date.year = inputDate[0];
        this.date.formatted = inputDate.value;
        console.log("AAAAAAAAAA >>>>>>", Date.parse(this.date.formatted), this.date.formatted);
        alert("Under construction! Please use the calendar to input date!");
        return;
      }

    }
    console.log(this.date);
    this.getDate(new Date(this.date.formatted));
    this.pickADate = !this.pickADate;
  }

  focusField() {
    (<any>document.getElementsByClassName("datepicker-input")[0]).focus();
  }

  initNewSl() {
    this.newSilverLining = new SilverLining({isPublic: true, user: this.dashboard.user});
  }

  hideAllTooltips() {
    $("[data-toggle='tooltip']").tooltip("hide");
  }

  initAllTooltips() {
    $('[data-toggle="tooltip"]').tooltip()
  }

  toggleEditor(save?: boolean) {
    this.hideAllTooltips();

    if (!this.inEditMode) {
      this.activeSilverLining = this.newSilverLining;
    }

    if (save) {
      if (this.editorOpen && this.inEditMode) {
        this.updateSilverLining();
      }
      else {
        this.addSilverLining(true);
      }
      tinyMCE.activeEditor.setContent('');
      this.initNewSl();
    }

    this.editorOpen = !this.editorOpen;
    if (this.editorOpen) {
      this.initEditor();
    }
    else {
      this.initAllTooltips();
    }
  }

  closeDetails() {
    this.initAllTooltips();
    this.activeSilverLining = null;
    this.detailsOpen = !this.detailsOpen;
  }

  updateSilverLining() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.activeSilverLining.description = tinyMCE.activeEditor.getContent();
    this.http.put(HOST + "/silver-linings/" +
      this.dashboard.currentDate.date.getUTCFullYear() + "/" +
      (this.dashboard.currentDate.date.getUTCMonth() + 1) + "/" +
      this.dashboard.currentDate.date.getUTCDate(),
      {"silver_lining": this.activeSilverLining.asJSON()},
      {headers: headers}
    )
      .map(res => res.json())
      .subscribe(data => {
        if (data) {
          this.dashboard.currentDate.silverLinings[0].id = data["id"];
          this.inProgress = false;
        }
      });
  }

  initEditor() {
    let self = this;
    tinymce.init({
      selector: '#tinymce-area',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
      image_advtab: true,
      templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ],
      height: "480",
      setup: function (editor) {
        // editor.setContent(self.activeSilverLining.description || '');
      }
    });

  }

  userHasHeartedThis(sl) {
    let found = -1;
    if (this.dashboard.user == null) return -1;
    for (let i = 0; i < sl.hearts.length; i++) {
      if (sl.hearts[i].userId == this.dashboard.user.id) {
        return i;
      }
    }
    return -1;
  }

  toggleSilverLiningHeart(sl: SilverLining) {
    if(this.dashboard.user.isAnonymous()){
      alert("You have to be logged in to <3 :) ");
      return;
    }
    let found = this.userHasHeartedThis(sl);

    if (found > -1) {
      this.unheartSilverLining(sl, found);
    }
    else {
      this.heartSilverLining(sl);
    }
  }

  heartSilverLining(sl: SilverLining) {
    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
    let options = new RequestOptions({ headers: headers });
    this.http.post(HOST + "/silver-lining-hearts/" + sl.id + "/" + this.dashboard.user.id, {}, options)
      .map(res => res.json())
      .subscribe(data => {
        sl.hearts.push(new SilverLiningHeart(data));
      });
  }

  unheartSilverLining(sl: SilverLining, heartIndex: number) {
    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
    let options = new RequestOptions({ headers: headers });
    this.http.post(HOST + "/silver-lining-hearts/" + sl.id + "/" + this.dashboard.user.id + "/delete", {}, options)
      .map(res => res.json())
      .subscribe(data => {
        sl.hearts.splice(heartIndex, 1);
      });
  }

  medium() {
    this.http.get(HOST + "/medium/authorize")
      .map(res => res.json())
      .subscribe(data => {
        console.log("RESPONSE: ", data);

      });
  }

  uploadFile() {
    // this.fileUpload = $event.target.files[0];
    if (this.fileUpload) {
      this.inProgress = true;
      let headers = new Headers();
      let formData: FormData = new FormData();

      formData.append('files', this.fileUpload, this.fileUpload.name);

      this.http.post(HOST + "/uploads",
        formData,
        {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          this.newSilverLining.attachment = new Attachment({name:this.fileUpload.name, filePath: HOST + data});
          /*if (this.fileUpload.name.indexOf('.txt') > 1 || this.fileUpload.name.indexOf('.pdf') > 1) {
            this.newSilverLining.description = '<a href="' + HOST + data + '">' + this.fileUpload.name + '</a>';
          }
          else {
            this.newSilverLining.description = '<img src="' + HOST + data + '" class="card-content"/>';
          }*/
          this.addSilverLining();
          this.uploadAFile = !this.uploadAFile;
          this.fileUpload = null;
          this.inProgress = false;
        });
    }
    else {
      alert("Please choose a file!")
    }
  }

  fileInputChanged($event) {
    this.fileUpload = $event.target.files[0];
    this.newSilverLining.description = $event.target.files[0].name;
    this.isAFileSl = true;
  }

  post() {
    if (this.fileUpload) {
      this.uploadFile()
    }
    else {
      this.addSilverLining()
    }
  }

  togglePublic(sl: SilverLining) {
    if (sl.user && sl.user.username == "anonymous") {
      alert("Please, login / register in order to be able to make something private :)! ")
      return;
    }
    sl.isPublic = !sl.isPublic;
  }

  showDetails(sl: SilverLining) {
    this.hideAllTooltips();
    this.detailsOpen = true;
    this.activeSilverLining = sl;
    this.mediumEditor = new MediumEditor('.editable');

  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.authSubscription.unsubscribe();
  }

  toggleUpload(){
    this.hideAllTooltips();
    this.uploadAFile = !this.uploadAFile;

    if(this.uploadAFile){
      this.initAllTooltips();
    }
  }
}
