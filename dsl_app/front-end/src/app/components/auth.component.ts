import {Component, Input, Output, EventEmitter, OnDestroy} from '@angular/core';
import {Auth} from '../services/auth.service'
import {User, CommonMessage, ActionType} from "../models";
import {Subscription} from "rxjs";

declare const $;

@Component({
  selector: 'auth',
  templateUrl: `../templates/auth.component.html`,
  styleUrls: ['../css/auth.component.css']
})
export class AuthComponent implements OnDestroy {
  user: User;
  @Input() loginOpen: boolean;
  @Output() onLoginOpenChange = new EventEmitter<boolean>();
  commonMsg: CommonMessage;
  message: string;
  isLogin: boolean;
  forgotPassword: boolean;
  subscription: Subscription;

  constructor(private auth: Auth) {
    this.user = new User();
    this.message = "";
    this.isLogin = true;
    this.forgotPassword = false;
    // http://jasonwatmore.com/post/2016/12/01/angular-2-communicating-between-components-with-observable-subject
    this.subscription = this.auth.getMessage().subscribe(commonMsg => {
      this.commonMsg = commonMsg;
      this.processMsg(commonMsg);
    });
  }

  processMsg(msg: CommonMessage) {
    switch (msg.action) {
      case ActionType.login:
        this.afterLogin(msg);
        break;
      case ActionType.register:
        this.afterRegister(msg);
        break;
      case ActionType.resetPassword:
        this.afterResetPassword(msg);
        break;
      default:
        console.warn("I got something I wasn't supposed to...", msg)
    }
  }

  afterLogin(msg: CommonMessage) {
    if (msg.success) {
      this.closeForm();
    }
    else {
      // show the msg to the user
      this.message = msg.message;
    }
  }

  afterRegister(msg: CommonMessage) {
    console.log("after register")
    if (msg.success) {
      this.message = msg.message; // "Thank you! Check your email and come here to login :)";
      this.toggleForm(true);
    }
    else {
      // show the msg to the user
      this.message = msg.message;
    }
  }

  afterResetPassword(msg: CommonMessage) {
    console.log("after reset")
    this.message = msg.message;
    if (msg.success) {
      this.toggleForm(true);
    }
  }

  login() {
    this.auth.login(this.user);
  }

  register() {
    this.auth.register(this.user)
  }

  resetPassword() {
    this.auth.resetPassword(this.user)
  }

  toggleForm(isLogin: boolean) {
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    this.isLogin = isLogin;
    this.forgotPassword = false;
  }

  toggleForgotPassword() {
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    this.forgotPassword = !this.forgotPassword;
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    this.resetMsg();
  }

  resetMsg() {
    this.message = "";
  }

  closeForm() {
    this.onLoginOpenChange.emit(!this.loginOpen);
    // this.loginOpen = !this.loginOpen;
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

}
