import {Component, AfterViewInit} from '@angular/core';

declare const $;

@Component({
  selector: 'summernote',
  templateUrl: `../templates/summernote-editor.component.html`,
  styleUrls: ['../css/summernote-editor.component.css']
})
export class SummerNoteEditorComponent implements AfterViewInit{
  ngAfterViewInit(): void {
    $('#summernote').summernote();
  }

  name = 'Angular';

}

