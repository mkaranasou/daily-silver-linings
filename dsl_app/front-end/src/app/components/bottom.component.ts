import { Component } from '@angular/core';

@Component({
  selector: 'bottom',
  templateUrl: `../templates/bottom.component.html`,
  styleUrls: ['../css/bottom.component.css']
})
export class BottomComponent {
  visibleAbout: boolean;
  name = 'Angular';

  constructor(){
    this.visibleAbout = false;
  }

  showInfo(){
    this.visibleAbout = !this.visibleAbout;
  }

}
