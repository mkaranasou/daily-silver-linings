import {
  Component, OnInit, Inject, Injectable, AfterViewInit, OnDestroy, Input, Output,
  EventEmitter
} from '@angular/core';
import {Http, HttpModule, Headers, RequestOptions} from '@angular/http';
import {
  Dashboard, User, Action, ActionType, OneGoodDay, SilverLining, SilverLiningHeart,
  CommonMessage
} from '../models';
import {DatePickerOptions, DateModel} from 'ng2-datepicker';
import {Auth} from '../services/auth.service'

declare const twemoji;
import 'rxjs/Rx';
import {Subscription} from "rxjs";
import {InProgressSvc} from "../services/inProgressSvc.service";

// declare const jQuery;
// declare const $;
// declare const tinymce;
// declare const tinyMCE;
// declare const HOST;
// declare const MediumEditor;
// declare const localStorage;

@Component({
  selector: 'silver-lining-details',
  templateUrl: '../templates/silver-lining-details.component.html',
  styleUrls: ["../css/silver-lining-details.component.css"],
  providers: [HttpModule]
})
@Injectable()
export class SilverLiningDetailsComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() activeSilverLining: SilverLining;
  @Input() user: User;
  @Output() onCloseDetails = new EventEmitter<boolean>();

  authSubscription: Subscription;
  commonMsg: CommonMessage;


  constructor(@Inject(Http) Http, private auth: Auth, private inProgressSvc: InProgressSvc) {

  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }
  userHasHeartedThis(sl) {
    let found = -1;
    if (this.user == null) return -1;
    for (let i = 0; i < sl.hearts.length; i++) {
      if (sl.hearts[i].userId == this.user.id) {
        return i;
      }
    }
    return -1;
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    // this.authSubscription.unsubscribe();
  }

  closeDetails(){
    this.onCloseDetails.emit(true);
  }

}
