import { Component } from '@angular/core';
import {Http, HttpModule, Headers, RequestOptions} from '@angular/http';

import {Auth} from '../services/auth.service'
import {User} from "../models";

@Component({
  selector: 'top',
  templateUrl: `../templates/top.component.html`,
  styleUrls: ['../css/top.component.css']
})
export class TopComponent {
  loginOpen:boolean;
  currentUser: User;

  constructor(private auth: Auth){
    this.loginOpen = false;
    this.getCurrentUser();
  }

  getCurrentUser(){
    if(this.auth.authenticated()){
      this.currentUser = new User(JSON.parse(localStorage.getItem("usr")));
    }
  }

  onLoginOpenChange(loginOpen: boolean) {
    this.loginOpen=loginOpen;
    // console.log("this.loginOpen", this.loginOpen, this.auth.currentUser)
    // this.currentUser = this.auth.currentUser;
    this.getCurrentUser();
  }
}
