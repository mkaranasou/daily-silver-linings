import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { DayStreamComponent }  from './components/day-stream.component';
import { TopComponent }  from './components/top.component';
import { BottomComponent }  from './components/bottom.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {SummerNoteEditorComponent} from "./components/summernote-editor.component";
import { DatePickerModule } from 'ng2-datepicker';
// import {AUTH_PROVIDERS, AuthHttp, AuthConfig} from 'angular2-jwt';
import {Auth} from './services/auth.service'
import { RouterModule, Routes } from '@angular/router';
import {AuthComponent} from "./components/auth.component";
import {InProgressSvc} from "./services/inProgressSvc.service";
import {SilverLiningDetailsComponent} from "./components/silver-lining-details.component";
import {UserSvc} from "./services/user.service";
import {DaySvc} from "./services/day.service";

const appRoutes: Routes = [
  { path: '', component: DayStreamComponent },
  { path: 'home', component: DayStreamComponent },
];


@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, DatePickerModule, RouterModule.forRoot(appRoutes)],
  declarations: [ AppComponent,
                  DayStreamComponent,
                  TopComponent,
                  BottomComponent,
                  SummerNoteEditorComponent,
                  AuthComponent,
                  SilverLiningDetailsComponent
                ],
  bootstrap:    [ AppComponent ],
  providers: [Auth, InProgressSvc, DaySvc, UserSvc]
})
export class AppModule { }
