/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {DayStreamComponent} from "app/components/day-stream.component";
import {TopComponent} from "./components/top.component";
import {BottomComponent} from "./components/bottom.component";
import {SummerNoteEditorComponent} from "./components/summernote-editor.component";
import {AuthComponent} from "app/components/auth.component";
import {SilverLiningDetailsComponent} from "app/components/silver-lining-details.component";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {DatePickerModule} from "ng2-datepicker";
import {RouterModule, Routes} from "@angular/router";
import {Auth} from "./services/auth.service";
import {InProgressSvc} from "./services/inProgressSvc.service";
import {DaySvc} from "./services/day.service";
import {UserSvc} from "app/services/user.service";
import {APP_BASE_HREF} from '@angular/common';

const appRoutes: Routes = [
  { path: '', component: DayStreamComponent },
  { path: 'home', component: DayStreamComponent },
];

const appTitle = "Daily Silver Linings";

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        DayStreamComponent,
        TopComponent,
        BottomComponent,
        SummerNoteEditorComponent,
        AuthComponent,
        SilverLiningDetailsComponent,
      ],
      imports:      [ BrowserModule, FormsModule, HttpModule, DatePickerModule, RouterModule.forRoot(appRoutes)],
      providers: [Auth, InProgressSvc, DaySvc, UserSvc, {provide: APP_BASE_HREF, useValue : '/' }]
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'Daily Silver linings'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual(appTitle);
  }));

  it('should have a navbar-brand that shows Daily Silver Linings', async(() => {
    const app = TestBed.createComponent(AppComponent);
    const top = TestBed.createComponent(TopComponent);
    const bottom = TestBed.createComponent(BottomComponent);
    const compiled = top.debugElement.nativeElement;
    top.detectChanges();
    expect(top).toBeTruthy();
    expect(compiled.querySelector('.daily-silver-linings').textContent).toEqual(appTitle);
  }));
  it('should have a hidden login when initializing', async(() => {
    const top = TestBed.createComponent(TopComponent);
    const compiled = top.debugElement.componentInstance;
    expect(compiled.loginOpen).toBe(false);
    //expect(compiled.querySelector('.daily-silver-linings')[0].textContent).toContain('Daily Silver linings');
  }));
  it('should have a "  Have a great day :)" at the bottom component', async(() => {
    const bottomMsg = '   Have a great day :)';
    const bottom = TestBed.createComponent(BottomComponent);
    const compiled = bottom.debugElement.nativeElement;
    bottom.detectChanges();
    expect(bottom).toBeTruthy();
    expect(compiled.querySelector('.navbar-brand').textContent).toEqual(bottomMsg);
  }));
  it('should have an input with "Write something good about this day :)" as a placeholder the day-stream component', async(() => {
    const bottomMsg = 'Write something good about this day :)';
    const app = TestBed.createComponent(AppComponent);
    const top = TestBed.createComponent(TopComponent);
    const dayStream = TestBed.createComponent(DayStreamComponent);
    const compiled = dayStream.debugElement.nativeElement;
    console.log(compiled, compiled.querySelector('#something-good-textarea').placeholder);
    dayStream.detectChanges();
    expect(dayStream).toBeTruthy();
    expect(compiled.querySelector('#something-good-textarea').placeholder).toEqual(bottomMsg);
  }));
});
