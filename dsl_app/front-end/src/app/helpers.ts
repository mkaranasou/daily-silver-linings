export class Helpers{
  static convertToCamelCase(dictData){
    let converted = {};
    for(let key in dictData){
      converted[key.replace(/(\_\w)/g, function (g) { return g[1].toUpperCase(); })] = dictData[key];
     /* if(dictData.hasOwnProperty(key)){ // not necessary since we're checking for json
      }*/
    }
    return converted;
  }
}
