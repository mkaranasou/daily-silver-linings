import {Injectable, Inject}      from '@angular/core';
import {BaseSvc, OneGoodDay, SilverLining} from "../models";
import {Http} from "@angular/http";
import {Auth} from "./auth.service";

@Injectable()
export class SilverLiningSvc extends BaseSvc{
  http: Http;

  constructor(@Inject(Http) Http, private auth: Auth) {
    super();
    this.http = Http;
    this.baseUrl = "/silver-linings/";
  }

  getSilverLiningsForDay(day:OneGoodDay){

  }

  getSilverLiningsForUser(){

  }

  addSilverLining(sl:SilverLining, isFileUpload){

  }

  editSilverLining(sl:SilverLining){

  }

  deleteSilverLining(sl:SilverLining){

  }

}
