import {Inject, Injectable}      from '@angular/core';
import {BaseSvc, OneGoodDay, User} from "../models";
import {RequestOptions, Http, RequestOptionsArgs} from "@angular/http";
import {Auth} from "./auth.service";


@Injectable()
export class DaySvc extends BaseSvc{
  currentDay: OneGoodDay;
  http: Http;

  constructor(@Inject(Http) Http, private auth: Auth) {
    super();
    this.http = Http;
    this.baseUrl = "/good-days/";
  }

  getDateUrl(date: Date, user:User) {
    // ask the backend with the utc date always
    let url = this.host + this.baseUrl + date.getUTCFullYear() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCDate();

    console.log(user);
    if (user && user.username != "anonymous") {
      url += "/" + user.id
    }
    return url;
  }

  getDate(user:User, date?: Date) {
    console.log(this.auth.authenticated());
    this.setInprogress();

    let today = date || new Date();

    // add authorization header with jwt token
    // let headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
    // let options = new RequestOptions({headers: headers}); // todo: for some reason this doesn't work..

    let url = this.getDateUrl(today, user);
    let self = this;

    return new Promise(function (resolve, reject) {
      self.http.get(url, <any>{headers: { 'Authorization': 'Bearer ' + self.auth.getToken() }})
        .map(
          res => res.json(),
          err => {
            console.error(err);
            self.done();
            reject(err);
          }
        )
        .subscribe(ogd => {
          self.done();
          resolve(ogd);
        });
    });
  }


}
