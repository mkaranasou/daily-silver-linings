import {Inject, Injectable}      from '@angular/core';
import {User, BaseSvc} from "../models";
import {Http} from "@angular/http";
import {Auth} from "./auth.service";

@Injectable()
export class SocketIOSvc  extends BaseSvc {
  user:User;
  private http:Http;

  constructor(private auth: Auth) {
    super();
    this.baseUrl = "/connection-established/";
  }
}
