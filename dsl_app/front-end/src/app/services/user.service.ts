import {Inject, Injectable}      from '@angular/core';
import {User, BaseSvc} from "../models";
import {Http} from "@angular/http";
import {Auth} from "./auth.service";

@Injectable()
export class UserSvc  extends BaseSvc {
  user:User;
  private http:Http;

  constructor(@Inject(Http) Http, private auth: Auth) {
    super();
    this.http = Http;
    this.baseUrl = "/users/";
  }

  getCurrentUser(){
    if (this.auth.authenticated()) {
        this.user = new User(JSON.parse(localStorage.getItem("usr")));
      }
      else{
      this.user = this.getAnonymousUser();
    }
    return this.user;
  }

  getAnonymousUser(){
    return new User({username: "anonymous"})
  }

  getUserByUsername(username:string){

  }

  getUserById(id: string){

  }

}
