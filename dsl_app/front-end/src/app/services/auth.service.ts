import {Inject, Injectable}      from '@angular/core';
import {tokenNotExpired} from 'angular2-jwt';
import {Http, HttpModule, Headers, RequestOptions} from '@angular/http';
import {CommonMessage, User, ActionType, BaseSvc} from "app/models";
import {Observable, Subject} from "rxjs";
import {InProgressSvc} from "./inProgressSvc.service";

// http://jasonwatmore.com/post/2016/12/01/angular-2-communicating-between-components-with-observable-subject
//https://github.com/auth0-samples/auth0-angularjs2-systemjs-sample


@Injectable()
export class Auth extends BaseSvc{
  http: any;
  currentUser: User;
  private commonMsg: Subject<CommonMessage>;

  constructor(@Inject(Http) Http, private inProgressSvc: InProgressSvc) {
    super();
    this.http = Http;
    this.currentUser = null;
    this.commonMsg = new Subject<CommonMessage>();
  }

  getMessage(): Observable<CommonMessage> {
    return this.commonMsg.asObservable();
  }

  sendMessage(message: CommonMessage) {
    this.commonMsg.next(message);
  }

  clearMessage() {
    this.commonMsg.next();
  }

  public login(user: User) {
    this.inProgressSvc.setInprogress();
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post(this.host + "/login",
      new CommonMessage({
        data: {
          "email": user.email,
          "password": user.password,
        }
      }),
      {headers: headers}
    )
      .map(res => res.json())
      .subscribe(
        data => {
          console.log(data);
          this.currentUser = new User(user);
          localStorage.setItem('token', data.data["token"]);
          localStorage.setItem('usr', JSON.stringify(data.data));
          data.action = ActionType.login;
          this.sendMessage(data);
          this.inProgressSvc.done();
        },
        err => {
          let data = JSON.parse(err._body);
          console.warn(err, data);
          data.action = ActionType.login;
          this.sendMessage(data);
          this.inProgressSvc.done();
        },
      );
  };

  public register(user: User) {
    this.inProgressSvc.setInprogress();
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post(this.host + "/register",
      new CommonMessage({
        data: {
          "email": user.email,
          "password": user.password,
          "username": user.username
        }
      }),
      {headers: headers}
    )
      .map(res => res.json())
      .subscribe(
        data => {
          console.log(data);
          // localStorage.setItem('token', data.data["token"]);
          data.action = ActionType.register;
          this.sendMessage(data);
          this.inProgressSvc.done();
        },
        err => {
          console.warn(err);
          let data = JSON.parse(err._body);
          data.action = ActionType.register;
          this.sendMessage(data);
          this.inProgressSvc.done();
        }
      );
  };

  public resetPassword(user: User) {
    this.inProgressSvc.setInprogress();
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post(this.host + "/forgot-password",
      new CommonMessage({
        data: {
          "email": user.email,
        }
      }),
      {headers: headers}
    )
      .map(res => res.json())
      .subscribe(
        data => {
          console.log(data);
          localStorage.setItem('token', data.data["token"]);
          data.action = ActionType.resetPassword;
          this.sendMessage(data);
          this.inProgressSvc.done();
        },
        err => {
          console.warn(err);
          let data = JSON.parse(err._body);
          data.action = ActionType.resetPassword;
          this.sendMessage(data);
          this.inProgressSvc.done();
        }
      );
  };

  private sendResponse(data) {
    this.sendMessage(new CommonMessage(data));
  }

  public authenticated() {
    // Check if there's an unexpired JWT
    // It searches for an item in localStorage with key == 'id_token'
    return tokenNotExpired();
  };

  public logout() {
    this.inProgressSvc.setInprogress();
    // Remove token from localStorage
    localStorage.removeItem('token');
    localStorage.removeItem('usr');
    this.sendMessage(new CommonMessage({success:true, action:ActionType.logout}));
    this.inProgressSvc.done();
  };

  public getToken(){
    return localStorage.getItem('token');
  }

}
