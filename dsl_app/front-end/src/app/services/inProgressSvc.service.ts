import {Injectable}      from '@angular/core';

@Injectable()
export class InProgressSvc {
  inProgress: boolean;

  constructor() {
    this.inProgress = false;
  }

  setInprogress(){
    this.inProgress = true;
  }

  done(){
    this.inProgress = false;
  }

  isInProgress(){
    return this.inProgress;
  }
}
