import {environment} from "../../environments/environment";

interface ServiceConfiguration {
    actionUrl: string,
}

const config = {
    HOST: environment.production? "mkaranasou.pythonanywhere.com": "localhost:5000",
    protocol: "http://",
};
const HOST = environment.production? "mkaranasou.pythonanywhere.com": "localhost:5000";

export const serviceConfig: ServiceConfiguration = {
    actionUrl: config.protocol + config.HOST,
};
