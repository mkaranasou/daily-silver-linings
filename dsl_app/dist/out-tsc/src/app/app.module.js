var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { DayStreamComponent } from './components/day-stream.component';
import { TopComponent } from './components/top.component';
import { BottomComponent } from './components/bottom.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SummerNoteEditorComponent } from "./components/summernote-editor.component";
import { DatePickerModule } from 'ng2-datepicker';
import { Auth } from './services/auth.service';
import { RouterModule } from '@angular/router';
import { AuthComponent } from "./components/auth.component";
import { InProgressSvc } from "./services/inProgressSvc.service";
import { SilverLiningDetailsComponent } from "./components/silver-lining-details.component";
import { UserSvc } from "./services/user.service";
import { DaySvc } from "./services/day.service";
var appRoutes = [
    { path: '', component: DayStreamComponent },
    { path: 'home', component: DayStreamComponent },
];
export var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            imports: [BrowserModule, FormsModule, HttpModule, DatePickerModule, RouterModule.forRoot(appRoutes)],
            declarations: [AppComponent,
                DayStreamComponent,
                TopComponent,
                BottomComponent,
                SummerNoteEditorComponent,
                AuthComponent,
                SilverLiningDetailsComponent
            ],
            bootstrap: [AppComponent],
            providers: [Auth, InProgressSvc, DaySvc, UserSvc]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=/Users/mariakaranasou/Projects/daily-silver-linings/dsl_app/front-end/src/src/app/app.module.js.map