/* tslint:disable:no-unused-variable */
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { DayStreamComponent } from "app/components/day-stream.component";
import { TopComponent } from "./components/top.component";
import { BottomComponent } from "./components/bottom.component";
import { SummerNoteEditorComponent } from "./components/summernote-editor.component";
import { AuthComponent } from "app/components/auth.component";
import { SilverLiningDetailsComponent } from "app/components/silver-lining-details.component";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { DatePickerModule } from "ng2-datepicker";
import { RouterModule } from "@angular/router";
import { Auth } from "./services/auth.service";
import { InProgressSvc } from "./services/inProgressSvc.service";
import { DaySvc } from "./services/day.service";
import { UserSvc } from "app/services/user.service";
import { APP_BASE_HREF } from '@angular/common';
var appRoutes = [
    { path: '', component: DayStreamComponent },
    { path: 'home', component: DayStreamComponent },
];
var appTitle = "Daily Silver Linings";
describe('AppComponent', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                DayStreamComponent,
                TopComponent,
                BottomComponent,
                SummerNoteEditorComponent,
                AuthComponent,
                SilverLiningDetailsComponent,
            ],
            imports: [BrowserModule, FormsModule, HttpModule, DatePickerModule, RouterModule.forRoot(appRoutes)],
            providers: [Auth, InProgressSvc, DaySvc, UserSvc, { provide: APP_BASE_HREF, useValue: '/' }]
        });
        TestBed.compileComponents();
    });
    it('should create the app', async(function () {
        var fixture = TestBed.createComponent(AppComponent);
        var app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it("should have as title 'Daily Silver linings'", async(function () {
        var fixture = TestBed.createComponent(AppComponent);
        var app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual(appTitle);
    }));
    it('should have a navbar-brand that shows Daily Silver Linings', async(function () {
        var app = TestBed.createComponent(AppComponent);
        var top = TestBed.createComponent(TopComponent);
        var bottom = TestBed.createComponent(BottomComponent);
        var compiled = top.debugElement.nativeElement;
        top.detectChanges();
        expect(top).toBeTruthy();
        expect(compiled.querySelector('.daily-silver-linings').textContent).toEqual(appTitle);
    }));
    it('should have a hidden login when initializing', async(function () {
        var top = TestBed.createComponent(TopComponent);
        var compiled = top.debugElement.componentInstance;
        expect(compiled.loginOpen).toBe(false);
        //expect(compiled.querySelector('.daily-silver-linings')[0].textContent).toContain('Daily Silver linings');
    }));
    it('should have a "  Have a great day :)" at the bottom component', async(function () {
        var bottomMsg = '   Have a great day :)';
        var bottom = TestBed.createComponent(BottomComponent);
        var compiled = bottom.debugElement.nativeElement;
        bottom.detectChanges();
        expect(bottom).toBeTruthy();
        expect(compiled.querySelector('.navbar-brand').textContent).toEqual(bottomMsg);
    }));
    it('should have an input with "Write something good about this day :)" as a placeholder the day-stream component', async(function () {
        var bottomMsg = 'Write something good about this day :)';
        var app = TestBed.createComponent(AppComponent);
        var top = TestBed.createComponent(TopComponent);
        var dayStream = TestBed.createComponent(DayStreamComponent);
        var compiled = dayStream.debugElement.nativeElement;
        console.log(compiled, compiled.querySelector('#something-good-textarea').placeholder);
        dayStream.detectChanges();
        expect(dayStream).toBeTruthy();
        expect(compiled.querySelector('#something-good-textarea').placeholder).toEqual(bottomMsg);
    }));
});
//# sourceMappingURL=/Users/mariakaranasou/Projects/daily-silver-linings/dsl_app/front-end/src/src/app/app.component.spec.js.map