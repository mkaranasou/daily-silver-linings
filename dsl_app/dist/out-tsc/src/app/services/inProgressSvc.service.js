var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
export var InProgressSvc = (function () {
    function InProgressSvc() {
        this.inProgress = false;
    }
    InProgressSvc.prototype.setInprogress = function () {
        this.inProgress = true;
    };
    InProgressSvc.prototype.done = function () {
        this.inProgress = false;
    };
    InProgressSvc.prototype.isInProgress = function () {
        return this.inProgress;
    };
    InProgressSvc = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [])
    ], InProgressSvc);
    return InProgressSvc;
}());
//# sourceMappingURL=/Users/mariakaranasou/Projects/daily-silver-linings/dsl_app/front-end/src/src/app/services/inProgressSvc.service.js.map