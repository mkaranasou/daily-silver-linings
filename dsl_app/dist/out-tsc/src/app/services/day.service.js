var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Inject, Injectable } from '@angular/core';
import { BaseSvc } from "../models";
import { Http } from "@angular/http";
import { Auth } from "./auth.service";
export var DaySvc = (function (_super) {
    __extends(DaySvc, _super);
    function DaySvc(Http, auth) {
        _super.call(this);
        this.auth = auth;
        this.http = Http;
        this.baseUrl = "/good-days/";
    }
    DaySvc.prototype.getDateUrl = function (date, user) {
        // ask the backend with the utc date always
        var url = this.host + this.baseUrl + date.getUTCFullYear() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCDate();
        console.log(user);
        if (user && user.username != "anonymous") {
            url += "/" + user.id;
        }
        return url;
    };
    DaySvc.prototype.getDate = function (user, date) {
        console.log(this.auth.authenticated());
        this.setInprogress();
        var today = date || new Date();
        // add authorization header with jwt token
        // let headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
        // let options = new RequestOptions({headers: headers}); // todo: for some reason this doesn't work..
        var url = this.getDateUrl(today, user);
        var self = this;
        return new Promise(function (resolve, reject) {
            self.http.get(url, { headers: { 'Authorization': 'Bearer ' + self.auth.getToken() } })
                .map(function (res) { return res.json(); }, function (err) {
                console.error(err);
                self.done();
                reject(err);
            })
                .subscribe(function (ogd) {
                self.done();
                resolve(ogd);
            });
        });
    };
    DaySvc = __decorate([
        Injectable(),
        __param(0, Inject(Http)), 
        __metadata('design:paramtypes', [Object, Auth])
    ], DaySvc);
    return DaySvc;
}(BaseSvc));
//# sourceMappingURL=/Users/mariakaranasou/Projects/daily-silver-linings/dsl_app/front-end/src/src/app/services/day.service.js.map