var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Inject, Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { Http, Headers } from '@angular/http';
import { CommonMessage, User, ActionType, BaseSvc } from "app/models";
import { Subject } from "rxjs";
import { InProgressSvc } from "./inProgressSvc.service";
// http://jasonwatmore.com/post/2016/12/01/angular-2-communicating-between-components-with-observable-subject
//https://github.com/auth0-samples/auth0-angularjs2-systemjs-sample
export var Auth = (function (_super) {
    __extends(Auth, _super);
    function Auth(Http, inProgressSvc) {
        _super.call(this);
        this.inProgressSvc = inProgressSvc;
        this.http = Http;
        this.currentUser = null;
        this.commonMsg = new Subject();
    }
    Auth.prototype.getMessage = function () {
        return this.commonMsg.asObservable();
    };
    Auth.prototype.sendMessage = function (message) {
        this.commonMsg.next(message);
    };
    Auth.prototype.clearMessage = function () {
        this.commonMsg.next();
    };
    Auth.prototype.login = function (user) {
        var _this = this;
        this.inProgressSvc.setInprogress();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(this.host + "/login", new CommonMessage({
            data: {
                "email": user.email,
                "password": user.password,
            }
        }), { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            _this.currentUser = new User(user);
            localStorage.setItem('token', data.data["token"]);
            localStorage.setItem('usr', JSON.stringify(data.data));
            data.action = ActionType.login;
            _this.sendMessage(data);
            _this.inProgressSvc.done();
        }, function (err) {
            var data = JSON.parse(err._body);
            console.warn(err, data);
            data.action = ActionType.login;
            _this.sendMessage(data);
            _this.inProgressSvc.done();
        });
    };
    ;
    Auth.prototype.register = function (user) {
        var _this = this;
        this.inProgressSvc.setInprogress();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(this.host + "/register", new CommonMessage({
            data: {
                "email": user.email,
                "password": user.password,
                "username": user.username
            }
        }), { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            // localStorage.setItem('token', data.data["token"]);
            data.action = ActionType.register;
            _this.sendMessage(data);
            _this.inProgressSvc.done();
        }, function (err) {
            console.warn(err);
            var data = JSON.parse(err._body);
            data.action = ActionType.register;
            _this.sendMessage(data);
            _this.inProgressSvc.done();
        });
    };
    ;
    Auth.prototype.resetPassword = function (user) {
        var _this = this;
        this.inProgressSvc.setInprogress();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(this.host + "/forgot-password", new CommonMessage({
            data: {
                "email": user.email,
            }
        }), { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            localStorage.setItem('token', data.data["token"]);
            data.action = ActionType.resetPassword;
            _this.sendMessage(data);
            _this.inProgressSvc.done();
        }, function (err) {
            console.warn(err);
            var data = JSON.parse(err._body);
            data.action = ActionType.resetPassword;
            _this.sendMessage(data);
            _this.inProgressSvc.done();
        });
    };
    ;
    Auth.prototype.sendResponse = function (data) {
        this.sendMessage(new CommonMessage(data));
    };
    Auth.prototype.authenticated = function () {
        // Check if there's an unexpired JWT
        // It searches for an item in localStorage with key == 'id_token'
        return tokenNotExpired();
    };
    ;
    Auth.prototype.logout = function () {
        this.inProgressSvc.setInprogress();
        // Remove token from localStorage
        localStorage.removeItem('token');
        localStorage.removeItem('usr');
        this.sendMessage(new CommonMessage({ success: true, action: ActionType.logout }));
        this.inProgressSvc.done();
    };
    ;
    Auth.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    Auth = __decorate([
        Injectable(),
        __param(0, Inject(Http)), 
        __metadata('design:paramtypes', [Object, InProgressSvc])
    ], Auth);
    return Auth;
}(BaseSvc));
//# sourceMappingURL=/Users/mariakaranasou/Projects/daily-silver-linings/dsl_app/front-end/src/src/app/services/auth.service.js.map