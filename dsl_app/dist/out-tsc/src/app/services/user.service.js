var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Inject, Injectable } from '@angular/core';
import { User, BaseSvc } from "../models";
import { Http } from "@angular/http";
import { Auth } from "./auth.service";
export var UserSvc = (function (_super) {
    __extends(UserSvc, _super);
    function UserSvc(Http, auth) {
        _super.call(this);
        this.auth = auth;
        this.http = Http;
        this.baseUrl = "/users/";
    }
    UserSvc.prototype.getCurrentUser = function () {
        if (this.auth.authenticated()) {
            this.user = new User(JSON.parse(localStorage.getItem("usr")));
        }
        else {
            this.user = this.getAnonymousUser();
        }
        return this.user;
    };
    UserSvc.prototype.getAnonymousUser = function () {
        return new User({ username: "anonymous" });
    };
    UserSvc.prototype.getUserByUsername = function (username) {
    };
    UserSvc.prototype.getUserById = function (id) {
    };
    UserSvc = __decorate([
        Injectable(),
        __param(0, Inject(Http)), 
        __metadata('design:paramtypes', [Object, Auth])
    ], UserSvc);
    return UserSvc;
}(BaseSvc));
//# sourceMappingURL=/Users/mariakaranasou/Projects/daily-silver-linings/dsl_app/front-end/src/src/app/services/user.service.js.map