export var Helpers = (function () {
    function Helpers() {
    }
    Helpers.convertToCamelCase = function (dictData) {
        var converted = {};
        for (var key in dictData) {
            converted[key.replace(/(\_\w)/g, function (g) { return g[1].toUpperCase(); })] = dictData[key];
        }
        return converted;
    };
    return Helpers;
}());
//# sourceMappingURL=/Users/mariakaranasou/Projects/daily-silver-linings/dsl_app/front-end/src/src/app/helpers.js.map