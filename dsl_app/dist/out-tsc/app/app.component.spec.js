/* tslint:disable:no-unused-variable */
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { DayStreamComponent } from "app/components/day-stream.component";
import { TopComponent } from "./components/top.component";
import { BottomComponent } from "./components/bottom.component";
import { SummerNoteEditorComponent } from "./components/summernote-editor.component";
import { AuthComponent } from "app/components/auth.component";
import { SilverLiningDetailsComponent } from "app/components/silver-lining-details.component";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { DatePickerModule } from "ng2-datepicker";
import { RouterModule } from "@angular/router";
import { Auth } from "./services/auth.service";
import { InProgressSvc } from "./services/inProgressSvc.service";
import { DaySvc } from "./services/day.service";
import { UserSvc } from "app/services/user.service";
describe('AppComponent', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                DayStreamComponent,
                TopComponent,
                BottomComponent,
                SummerNoteEditorComponent,
                AuthComponent,
                SilverLiningDetailsComponent,
            ],
            imports: [BrowserModule, FormsModule, HttpModule, DatePickerModule, RouterModule],
            providers: [Auth, InProgressSvc, DaySvc, UserSvc]
        });
        TestBed.compileComponents();
    });
    it('should create the app', async(function () {
        var fixture = TestBed.createComponent(AppComponent);
        var app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it("should have as title 'app works!'", async(function () {
        var fixture = TestBed.createComponent(AppComponent);
        var app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('Daily Silver linings');
    }));
    it('should have a navbar-brand that shows Daily Silver Linings', async(function () {
        var fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        var compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('.navbar-brand')[0].textContent).toContain('Daily Silver linings');
    }));
});
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/app.component.spec.js.map