var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Inject, Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { Http, Headers } from '@angular/http';
import { CommonMessage, User, ActionType } from "app/models";
import { Subject } from "rxjs";
import { InProgressSvc } from "./inProgressSvc.service";
var Auth = (function () {
    function Auth(Http, inProgress) {
        this.inProgress = inProgress;
        // Configure Auth0
        // lock = new Auth0Lock('9vy6f70FeulVVW1cOfhoNxirl79TTj28', 'digitalsense.eu.auth0.com', {});
        this.lock = new Auth0Lock('9vy6f70FeulVVW1cOfhoNxirl79TTj28', 'digitalsense.eu.auth0.com', {
            auth: {
                redirectUrl: 'http://localhost:5000/callbacks/auth0',
                responseType: 'code',
                params: {
                    scope: 'openid email' // Learn about scopes: https://auth0.com/docs/scopes
                }
            }
        });
        /* // Add callback for lock `authenticated` event
         this.lock.on('authenticated', (authResult) => {
         console.log(authResult);
         localStorage.setItem('id_token', authResult.idToken);
         });*/
        this.http = Http;
        this.currentUser = null;
        this.commonMsg = new Subject();
    }
    Auth.prototype.getMessage = function () {
        console.log("GET MESSAGE");
        return this.commonMsg.asObservable();
    };
    Auth.prototype.sendMessage = function (message) {
        this.commonMsg.next(message);
    };
    Auth.prototype.clearMessage = function () {
        this.commonMsg.next();
    };
    Auth.prototype.login = function (user) {
        var _this = this;
        this.inProgress.setInprogress();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(HOST + "/login", new CommonMessage({
            data: {
                "email": user.email,
                "password": user.password,
            }
        }), { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            // data = JSON.parse(data._body);
            _this.currentUser = new User(user);
            localStorage.setItem('token', data.data["token"]);
            localStorage.setItem('usr', JSON.stringify(data.data));
            data.action = ActionType.login;
            _this.sendMessage(data);
            _this.inProgress.done();
        }, function (err) {
            var data = JSON.parse(err._body);
            console.warn(err, data);
            data.action = ActionType.login;
            _this.sendMessage(data);
            _this.inProgress.done();
        });
    };
    ;
    Auth.prototype.register = function (user) {
        var _this = this;
        this.inProgress.setInprogress();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(HOST + "/register", new CommonMessage({
            data: {
                "email": user.email,
                "password": user.password,
                "username": user.username
            }
        }), { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            // localStorage.setItem('token', data.data["token"]);
            data.action = ActionType.register;
            _this.sendMessage(data);
            _this.inProgress.done();
        }, function (err) {
            console.warn(err);
            var data = JSON.parse(err._body);
            data.action = ActionType.register;
            _this.sendMessage(data);
            _this.inProgress.done();
        });
    };
    ;
    Auth.prototype.resetPassword = function (user) {
        var _this = this;
        this.inProgress.setInprogress();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(HOST + "/forgot-password", new CommonMessage({
            data: {
                "email": user.email,
            }
        }), { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            localStorage.setItem('token', data.data["token"]);
            data.action = ActionType.resetPassword;
            _this.sendMessage(data);
            _this.inProgress.done();
        }, function (err) {
            console.warn(err);
            var data = JSON.parse(err._body);
            data.action = ActionType.resetPassword;
            _this.sendMessage(data);
            _this.inProgress.done();
        });
    };
    ;
    Auth.prototype.sendResponse = function (data) {
        this.sendMessage(new CommonMessage(data));
    };
    Auth.prototype.authenticated = function () {
        // Check if there's an unexpired JWT
        // It searches for an item in localStorage with key == 'id_token'
        return tokenNotExpired();
    };
    ;
    Auth.prototype.logout = function () {
        this.inProgress.setInprogress();
        // Remove token from localStorage
        localStorage.removeItem('token');
        localStorage.removeItem('usr');
        this.sendMessage(new CommonMessage({ success: true, action: ActionType.logout }));
        this.inProgress.done();
    };
    ;
    Auth.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    return Auth;
}());
Auth = __decorate([
    Injectable(),
    __param(0, Inject(Http)),
    __metadata("design:paramtypes", [Object, InProgressSvc])
], Auth);
export { Auth };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/services/auth.service.js.map