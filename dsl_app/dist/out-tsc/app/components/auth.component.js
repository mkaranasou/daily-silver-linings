var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Auth } from '../services/auth.service';
import { User, ActionType } from "../models";
var AuthComponent = (function () {
    function AuthComponent(auth) {
        var _this = this;
        this.auth = auth;
        this.onLoginOpenChange = new EventEmitter();
        this.user = new User();
        this.message = "";
        this.isLogin = true;
        this.forgotPassword = false;
        // http://jasonwatmore.com/post/2016/12/01/angular-2-communicating-between-components-with-observable-subject
        this.subscription = this.auth.getMessage().subscribe(function (commonMsg) {
            _this.commonMsg = commonMsg;
            _this.processMsg(commonMsg);
        });
    }
    AuthComponent.prototype.processMsg = function (msg) {
        switch (msg.action) {
            case ActionType.login:
                this.afterLogin(msg);
                break;
            case ActionType.register:
                this.afterRegister(msg);
                break;
            case ActionType.resetPassword:
                this.afterResetPassword(msg);
                break;
            default:
                console.warn("I got something I wasn't supposed to...", msg);
        }
    };
    AuthComponent.prototype.afterLogin = function (msg) {
        if (msg.success) {
            this.closeForm();
        }
        else {
            // show the msg to the user
            this.message = msg.message;
        }
    };
    AuthComponent.prototype.afterRegister = function (msg) {
        console.log("after register");
        if (msg.success) {
            this.message = msg.message; // "Thank you! Check your email and come here to login :)";
            this.toggleForm(true);
        }
        else {
            // show the msg to the user
            this.message = msg.message;
        }
    };
    AuthComponent.prototype.afterResetPassword = function (msg) {
        console.log("after reset");
        this.message = msg.message;
        if (msg.success) {
            this.toggleForm(true);
        }
    };
    AuthComponent.prototype.login = function () {
        this.auth.login(this.user);
    };
    AuthComponent.prototype.register = function () {
        this.auth.register(this.user);
    };
    AuthComponent.prototype.resetPassword = function () {
        this.auth.resetPassword(this.user);
    };
    AuthComponent.prototype.toggleForm = function (isLogin) {
        $('form').animate({ height: "toggle", opacity: "toggle" }, "slow");
        $('form').animate({ height: "toggle", opacity: "toggle" }, "slow");
        this.isLogin = isLogin;
        this.forgotPassword = false;
    };
    AuthComponent.prototype.toggleForgotPassword = function () {
        $('form').animate({ height: "toggle", opacity: "toggle" }, "slow");
        this.forgotPassword = !this.forgotPassword;
        $('form').animate({ height: "toggle", opacity: "toggle" }, "slow");
        this.resetMsg();
    };
    AuthComponent.prototype.resetMsg = function () {
        this.message = "";
    };
    AuthComponent.prototype.closeForm = function () {
        this.onLoginOpenChange.emit(!this.loginOpen);
        // this.loginOpen = !this.loginOpen;
    };
    AuthComponent.prototype.ngOnDestroy = function () {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    };
    return AuthComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], AuthComponent.prototype, "loginOpen", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], AuthComponent.prototype, "onLoginOpenChange", void 0);
AuthComponent = __decorate([
    Component({
        selector: 'auth',
        templateUrl: "../templates/auth.component.html",
        styleUrls: ['../css/auth.component.css']
    }),
    __metadata("design:paramtypes", [Auth])
], AuthComponent);
export { AuthComponent };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/components/auth.component.js.map