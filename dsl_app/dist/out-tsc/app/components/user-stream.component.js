/*
import {Component, OnInit, Inject, Injectable, AfterViewInit} from '@angular/core';
import {Http, HttpModule, Headers, RequestOptions} from '@angular/http';
import {Dashboard, User, Action, ActionType, OneGoodDay, SilverLining, SilverLiningHeart} from '../models';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import {Auth} from '../services/auth.service'

declare const twemoji;
import 'rxjs/Rx';

declare const jQuery;
declare const tinymce;
declare const tinyMCE;
declare const HOST;
declare const MediumEditor;
declare const localStorage;

@Component({
  selector: 'main-container',
  templateUrl: '../templates/day-stream.component.html',
  styleUrls: ["../css/user-stream.component.css"],
  providers: [HttpModule, Auth]
})
@Injectable()
export class UserStreamComponent implements OnInit{
  dashboard:Dashboard;
  actions: Action[];
  currentDate: string;
  chosenDate: Date;
  newSilverLining: string;
  http: any;
  editorOpen: boolean;
  date: DateModel;
  options: DatePickerOptions;
  pickADate: boolean;
  uploadAFile: boolean;
  inProgress: boolean;
  detailsOpen: boolean;
  fileUpload: File;
  datePickerOptions: any;
  activeSilverLining: SilverLining;
  mediumEditor: any;


  constructor(@Inject(Http) Http, private auth: Auth){
    this.http = Http;
    this.currentDate = OneGoodDay.getStrDate(new Date());
    this.chosenDate = new Date();
    this.dashboard = new Dashboard();
    this.dashboard.user = new User();
    this.newSilverLining = "";
    this.editorOpen = false;
    this.options = new DatePickerOptions();
    this.pickADate = false;
    this.uploadAFile = false;
    this.inProgress = false;
    this.detailsOpen = false;
    this.fileUpload = null;
    this.actions = [];
    this.activeSilverLining = null;
    this.mediumEditor = null;
    // due to issue of date - day not displaying correctly:
    // https://github.com/jkuri/ng2-datepicker/issues/111
    this.datePickerOptions = {
            maxDate: new Date(),
            firstWeekdaySunday: true
        };
  }

  ngOnInit(): void {
    this.getDate();

  }

  getDate(date?: Date){
    let today = date || new Date();
    console.log(HOST + "/good-days/" +today.getUTCFullYear() + "/" + (today.getUTCMonth() + 1) + "/" + today.getUTCDate());
    this.http.get(HOST + "/good-days/"+
                  today.getUTCFullYear() + "/" +
                  (today.getUTCMonth() + 1) + "/" +
                  today.getUTCDate())
      .map(res => res.json())
      .subscribe(ogd =>{
        console.log(ogd);
        this.dashboard.currentDate = new OneGoodDay({"date":new Date(ogd["date"]),
                                                    "isPublic": ogd["is_public"],
                                                    "silverLinings": ogd["silver_linings"]
                                                  })
        this.storeCurrentDate( this.dashboard.currentDate.dateStr);
      });

  }

  getStoredDate(){
    return localStorage.getItem("current_view_date");
  }


  storeCurrentDate(date: string){
    return localStorage.setItem("current_view_date", date);
  }

  addSilverLining(fromEditor?:boolean){
    if(fromEditor){
      this.newSilverLining = tinyMCE.activeEditor.getContent();
    }
    if(this.newSilverLining == null || this.newSilverLining.length < 2){
      this.newSilverLining = "";
      alert("Please enter something here :) ");
      return;
    }
    this.inProgress = true;
    // this.newSilverLining = twemoji.parse(this.newSilverLining);
    this.dashboard.currentDate.silverLinings.unshift(
      new SilverLining({user:this.dashboard.user,
                        description:this.newSilverLining,
                        isApproved: false
                      })
    );
    this.activeSilverLining = this.dashboard.currentDate.silverLinings[0];
    this.newSilverLining = "";
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post(HOST + "/silver-linings/"+
                  this.dashboard.currentDate.date.getUTCFullYear() + "/" +
                  (this.dashboard.currentDate.date.getUTCMonth() + 1) + "/" +
                  this.dashboard.currentDate.date.getUTCDate(),
                  {"new_silver_lining": this.dashboard.currentDate.silverLinings[0].asJSON()},
                  { headers: headers }
                  )
      .map(res => res.json())
      .subscribe(data =>{
        console.log(data);
        if(data){
          this.dashboard.currentDate.silverLinings[0].isApproved = true;
          this.dashboard.currentDate.silverLinings[0].id = data["id"];
          this.inProgress = false;
        }
      });

  }

  editSilverLining(sl: SilverLining){
    this.editorOpen = !this.editorOpen;
    if(!tinyMCE.activeEditor){
      this.initEditor()
    }
    tinyMCE.activeEditor.setContent(sl.description);
  }

  deleteSilverLining(sl: SilverLining){
    sl.isApproved = false;
    this.http.post(HOST + "/silver-linings/"+ sl.id + "/delete")
      .map(res => res.json())
      .subscribe(data =>{
        console.log(data);
        let index = this.dashboard.currentDate.silverLinings.indexOf(sl);
        this.dashboard.currentDate.silverLinings.splice(index, 1);
      });

  }

  removeSilverLining(sl:SilverLining){
    if(this.dashboard.user==sl.user){
      let index=this.dashboard.currentDate.silverLinings.indexOf(sl);
      this.dashboard.currentDate.silverLinings.splice(index, 1);
    }
  }

  goToPreviousDay(){
    let d =  new Date(this.dashboard.currentDate.date.getTime());
    this.getDate(new Date(d.setDate(d.getDate()-1)))
  }

  goToNextDay(){
    let d =  new Date(this.dashboard.currentDate.date.getTime());
    this.getDate(new Date(d.setDate(d.getDate()+1)))
  }

  applyDate(){
    this.getDate(new Date(this.date.formatted));
    this.pickADate = !this.pickADate;
  }

  focusField(){
    console.log(document.getElementsByClassName("datepicker-input")[0]);
    (<any>document.getElementsByClassName("datepicker-input")[0]).focus();
  }

  toggleEditor(save?:boolean){
    if(save){
      this.addSilverLining(true);
    }
    this.editorOpen = !this.editorOpen;
    if(this.editorOpen){
     this.initEditor()
    }
  }

  initEditor(){
   /!* tinymce.init({
      selector:'#tinymce-area',
      theme: 'inlite',
      plugins: "emoticons",
      toolbar: "emoticons",
      height : "480",
    });*!/
   tinymce.init({
     selector:'#tinymce-area',
     plugins: [
       'advlist autolink lists link image charmap print preview hr anchor pagebreak',
       'searchreplace wordcount visualblocks visualchars code fullscreen',
       'insertdatetime media nonbreaking save table contextmenu directionality',
       'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
     ],
     toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
     toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
     image_advtab: true,
     templates: [
       { title: 'Test template 1', content: 'Test 1' },
       { title: 'Test template 2', content: 'Test 2' }
     ],
     content_css: [
       '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
       '//www.tinymce.com/css/codepen.min.css'
     ],
     height : "480",
   })
  }

  heartSilverLining(sl:SilverLining){
    this.http.post(HOST + "/silver-lining-hearts/"+ sl.id + "/" + 1)
      .map(res => res.json())
      .subscribe(data =>{
        console.log(data);
        sl.hearts.push(new SilverLiningHeart());

        // let index = this.dashboard.currentDate.silverLinings.indexOf(sl);
        // this.dashboard.currentDate.silverLinings.splice(index, 1);

        this.http.post(HOST + "/silver-lining-hearts/"+ sl.id + "/" + 1 + "/delete")
          .map(res => res.json())
          .subscribe(data =>{
            console.log(data);
            sl.hearts.splice(0, 1);
            // let index = this.dashboard.currentDate.silverLinings.indexOf(sl);
            // this.dashboard.currentDate.silverLinings.splice(index, 1);
          });
      });
  }

  medium(){
    this.http.get(HOST + "/medium/authorize")
      .map(res => res.json())
      .subscribe(data =>{
        console.log("RESPONSE: ", data);

      });
  }

  uploadFile(){
    // this.fileUpload = $event.target.files[0];
    console.log(this.fileUpload);
    if (this.fileUpload){
      this.inProgress = true;
      let headers = new Headers();
      let formData:FormData = new FormData();

      formData.append('files', this.fileUpload, this.fileUpload.name);

      this.http.post(HOST + "/uploads",
        formData,
        { headers: headers })
        .map(res => res.json())
        .subscribe(data =>{
          console.log("RESPONSE: ", data);
          if(this.fileUpload.name.indexOf('.txt') > 1 || this.fileUpload.name.indexOf('.pdf') > 1){
           this.newSilverLining = '<a href="'+ HOST + data + '">'+ this.fileUpload.name + '</a>';
          }
          else{
            this.newSilverLining = '<img src="'+ HOST + data + '" class="card-content"/>';
          }
          this.addSilverLining();
          this.uploadAFile = !this.uploadAFile;
          this.fileUpload = null;
          this.inProgress = false;
        });
    }
    else{
      alert("Please choose a file!")
    }
  }
  fileInputChanged($event){
    this.fileUpload = $event.target.files[0];
  }

  post(){
    if(this.uploadAFile){
      this.uploadFile()
    }
    else{
      this.addSilverLining()
    }
  }

  showDetails(sl: SilverLining){
    this.detailsOpen = true;
    this.activeSilverLining = sl;
    this.mediumEditor = new MediumEditor('.editable');

  }
}
*/
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/components/user-stream.component.js.map