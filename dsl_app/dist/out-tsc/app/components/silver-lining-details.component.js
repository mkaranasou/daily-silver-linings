var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject, Injectable, Input, Output, EventEmitter } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { User, SilverLining } from '../models';
import { Auth } from '../services/auth.service';
import 'rxjs/Rx';
import { InProgressSvc } from "../services/inProgressSvc.service";
// declare const jQuery;
// declare const $;
// declare const tinymce;
// declare const tinyMCE;
// declare const HOST;
// declare const MediumEditor;
// declare const localStorage;
var SilverLiningDetailsComponent = (function () {
    function SilverLiningDetailsComponent(Http, auth, inProgressSvc) {
        this.auth = auth;
        this.inProgressSvc = inProgressSvc;
        this.onCloseDetails = new EventEmitter();
    }
    SilverLiningDetailsComponent.prototype.ngOnInit = function () {
    };
    SilverLiningDetailsComponent.prototype.ngAfterViewInit = function () {
    };
    SilverLiningDetailsComponent.prototype.userHasHeartedThis = function (sl) {
        var found = -1;
        if (this.user == null)
            return -1;
        for (var i = 0; i < sl.hearts.length; i++) {
            if (sl.hearts[i].userId == this.user.id) {
                return i;
            }
        }
        return -1;
    };
    SilverLiningDetailsComponent.prototype.ngOnDestroy = function () {
        // unsubscribe to ensure no memory leaks
        // this.authSubscription.unsubscribe();
    };
    SilverLiningDetailsComponent.prototype.closeDetails = function () {
        this.onCloseDetails.emit(true);
    };
    return SilverLiningDetailsComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", SilverLining)
], SilverLiningDetailsComponent.prototype, "activeSilverLining", void 0);
__decorate([
    Input(),
    __metadata("design:type", User)
], SilverLiningDetailsComponent.prototype, "user", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], SilverLiningDetailsComponent.prototype, "onCloseDetails", void 0);
SilverLiningDetailsComponent = __decorate([
    Component({
        selector: 'silver-lining-details',
        templateUrl: '../templates/silver-lining-details.component.html',
        styleUrls: ["../css/silver-lining-details.component.css"],
        providers: [HttpModule]
    }),
    Injectable(),
    __param(0, Inject(Http)),
    __metadata("design:paramtypes", [Object, Auth, InProgressSvc])
], SilverLiningDetailsComponent);
export { SilverLiningDetailsComponent };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/components/silver-lining-details.component.js.map