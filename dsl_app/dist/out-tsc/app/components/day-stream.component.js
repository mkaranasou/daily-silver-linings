var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject, Injectable } from '@angular/core';
import { Http, HttpModule, Headers, RequestOptions } from '@angular/http';
import { Dashboard, ActionType, OneGoodDay, SilverLining, SilverLiningHeart } from '../models';
import { DatePickerOptions, DateModel } from 'ng2-datepicker';
import { Auth } from '../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/Rx';
import { InProgressSvc } from "../services/inProgressSvc.service";
import { DaySvc } from "../services/day.service";
import { UserSvc } from "../services/user.service";
var DayStreamComponent = (function () {
    function DayStreamComponent(Http, auth, inProgressSvc, daySvc, userSvc, activatedRoute) {
        this.auth = auth;
        this.inProgressSvc = inProgressSvc;
        this.daySvc = daySvc;
        this.userSvc = userSvc;
        this.activatedRoute = activatedRoute;
        this.http = Http;
        this.currentDate = OneGoodDay.getStrDate(new Date());
        this.chosenDate = new Date();
        this.dashboard = new Dashboard();
        this.dashboard.user = null;
        this.newSilverLining = new SilverLining({ isPublic: true });
        this.editorOpen = false;
        this.inEditMode = false;
        this.options = new DatePickerOptions();
        this.pickADate = false;
        this.uploadAFile = false;
        this.inProgress = this.inProgressSvc.isInProgress();
        this.detailsOpen = false;
        this.fileUpload = null;
        this.actions = [];
        this.activeSilverLining = null;
        this.mediumEditor = null;
        // due to issue of date - day not displaying correctly:
        // https://github.com/jkuri/ng2-datepicker/issues/111
        this.datePickerOptions = {
            maxDate: new Date(),
            firstWeekdaySunday: true
        };
    }
    DayStreamComponent.prototype.processMsg = function (msg) {
        switch (msg.action) {
            case ActionType.logout:
            case ActionType.login:
                this.afterAuth(msg);
                break;
            case ActionType.register:
                break;
            case ActionType.resetPassword:
                break;
            default:
                console.warn("I got something I wasn't supposed to...", msg);
        }
    };
    DayStreamComponent.prototype.afterAuth = function (msg) {
        if (msg.success) {
            this.getCurrentUser();
            this.getDate();
            this.initNewSl();
        }
    };
    DayStreamComponent.prototype.onCloseDetails = function (close) {
        this.detailsOpen = false;
    };
    DayStreamComponent.prototype.ngOnInit = function () {
        var _this = this;
        // subscribe to router event
        this.activatedRoute.params.subscribe(function (params) {
            console.log("params", params, params["year"]);
        });
        var self = this;
        this.inProgressSvc.setInprogress();
        this.authSubscription = this.auth.getMessage().subscribe(function (commonMsg) {
            _this.commonMsg = commonMsg;
            _this.processMsg(commonMsg);
        });
        this.getCurrentUser().then(function () {
            self.getDate().then(function () {
                self.initNewSl();
                self.inProgressSvc.done();
            });
        });
    };
    DayStreamComponent.prototype.ngAfterViewInit = function () {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    };
    DayStreamComponent.prototype.getCurrentUser = function () {
        var self = this;
        this.dashboard.user = null;
        return new Promise(function (resolve, reject) {
            self.dashboard.user = self.userSvc.getCurrentUser();
            resolve();
        });
    };
    DayStreamComponent.prototype.getDate = function (date) {
        var self = this;
        return this.daySvc.getDate(this.dashboard.user, date).then(function (ogd) {
            self.dashboard.currentDate = new OneGoodDay(ogd);
            self.storeCurrentDate(self.dashboard.currentDate.dateStr);
        });
    };
    DayStreamComponent.prototype.getStoredDate = function () {
        return localStorage.getItem("current_view_date");
    };
    DayStreamComponent.prototype.storeCurrentDate = function (date) {
        return localStorage.setItem("current_view_date", date);
    };
    DayStreamComponent.prototype.addSilverLining = function (fromEditor) {
        var _this = this;
        if (this.userSvc.user.isAnonymous()) {
            alert("Please login/register to post :) ");
            return;
        }
        if (fromEditor) {
            if (this.inEditMode) {
                this.newSilverLining = this.activeSilverLining;
            }
            this.newSilverLining.description = tinyMCE.activeEditor.getContent();
        }
        if (this.newSilverLining.description == null || this.newSilverLining.description.length < 2) {
            this.newSilverLining.description = "";
            alert("Please enter something here :) ");
            return;
        }
        this.inProgress = true;
        this.dashboard.currentDate.silverLinings.unshift(new SilverLining({
            user: this.dashboard.user,
            description: this.newSilverLining.description,
            isApproved: false,
            isPublic: this.newSilverLining.isPublic || this.dashboard.user.username == "anonymous"
        }));
        this.activeSilverLining = this.dashboard.currentDate.silverLinings[0];
        this.newSilverLining.description = "";
        // add authorization header with jwt token
        var headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
        headers.append('Content-Type', 'application/json');
        var options = new RequestOptions({ headers: headers });
        this.http.post(HOST + "/silver-linings/" +
            this.dashboard.currentDate.date.getUTCFullYear() + "/" +
            (this.dashboard.currentDate.date.getUTCMonth() + 1) + "/" +
            this.dashboard.currentDate.date.getUTCDate(), { "new_silver_lining": this.dashboard.currentDate.silverLinings[0].asJSON() }, { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            if (data) {
                // this.dashboard.currentDate.silverLinings[0].isApproved = true;
                _this.dashboard.currentDate.silverLinings[0].id = data["id"];
                _this.inProgress = false;
            }
        });
    };
    DayStreamComponent.prototype.editSilverLining = function (sl) {
        this.hideAllTooltips();
        this.inEditMode = true;
        this.activeSilverLining = sl;
        console.warn(this.activeSilverLining, sl);
        this.editorOpen = !this.editorOpen;
        if (!tinyMCE.activeEditor) {
            this.initEditor();
        }
        tinyMCE.activeEditor.setContent(sl.description);
    };
    DayStreamComponent.prototype.deleteSilverLining = function (sl) {
        var _this = this;
        // add authorization header with jwt token
        var headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
        headers.append('Content-Type', 'application/json');
        var options = new RequestOptions({ headers: headers });
        sl.isApproved = false;
        this.http.post(HOST + "/silver-linings/" + sl.id + "/delete", {}, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            var index = _this.dashboard.currentDate.silverLinings.indexOf(sl);
            _this.dashboard.currentDate.silverLinings.splice(index, 1);
        });
    };
    DayStreamComponent.prototype.goToPreviousDay = function () {
        var d = new Date(this.dashboard.currentDate.date.getTime());
        this.getDate(new Date(d.setDate(d.getDate() - 1)));
    };
    DayStreamComponent.prototype.goToNextDay = function () {
        var d = new Date(this.dashboard.currentDate.date.getTime());
        this.getDate(new Date(d.setDate(d.getDate() + 1)));
    };
    DayStreamComponent.prototype.applyDate = function () {
        if (!this.date) {
            var inputDate = document.getElementsByClassName("datepicker-input")[0];
            this.date = new DateModel();
            console.log(inputDate, inputDate.value);
            if (inputDate.value != undefined) {
                // inputDate = inputDate.value.split(/-/g);
                // this.date.day = inputDate[2];
                // this.date.month = inputDate[1];
                // this.date.year = inputDate[0];
                this.date.formatted = inputDate.value;
                console.log("AAAAAAAAAA >>>>>>", Date.parse(this.date.formatted), this.date.formatted);
                alert("Under construction! Please use the calendar to input date!");
                return;
            }
        }
        console.log(this.date);
        this.getDate(new Date(this.date.formatted));
        this.pickADate = !this.pickADate;
    };
    DayStreamComponent.prototype.focusField = function () {
        document.getElementsByClassName("datepicker-input")[0].focus();
    };
    DayStreamComponent.prototype.initNewSl = function () {
        this.newSilverLining = new SilverLining({ isPublic: true, user: this.dashboard.user });
    };
    DayStreamComponent.prototype.hideAllTooltips = function () {
        $("[data-toggle='tooltip']").tooltip("hide");
    };
    DayStreamComponent.prototype.initAllTooltips = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };
    DayStreamComponent.prototype.toggleEditor = function (save) {
        this.hideAllTooltips();
        if (!this.inEditMode) {
            this.activeSilverLining = this.newSilverLining;
        }
        if (save) {
            if (this.editorOpen && this.inEditMode) {
                this.updateSilverLining();
            }
            else {
                this.addSilverLining(true);
            }
            tinyMCE.activeEditor.setContent('');
            this.initNewSl();
        }
        this.editorOpen = !this.editorOpen;
        if (this.editorOpen) {
            this.initEditor();
        }
        else {
            console.log("LALALAL");
            this.initAllTooltips();
        }
    };
    DayStreamComponent.prototype.closeDetails = function () {
        this.initAllTooltips();
        this.activeSilverLining = null;
        this.detailsOpen = !this.detailsOpen;
    };
    DayStreamComponent.prototype.updateSilverLining = function () {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.activeSilverLining.description = tinyMCE.activeEditor.getContent();
        this.http.put(HOST + "/silver-linings/" +
            this.dashboard.currentDate.date.getUTCFullYear() + "/" +
            (this.dashboard.currentDate.date.getUTCMonth() + 1) + "/" +
            this.dashboard.currentDate.date.getUTCDate(), { "silver_lining": this.activeSilverLining.asJSON() }, { headers: headers })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            if (data) {
                _this.dashboard.currentDate.silverLinings[0].id = data["id"];
                _this.inProgress = false;
            }
        });
    };
    DayStreamComponent.prototype.initEditor = function () {
        var self = this;
        tinymce.init({
            selector: '#tinymce-area',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            height: "480",
            setup: function (editor) {
                // editor.setContent(self.activeSilverLining.description || '');
            }
        });
    };
    DayStreamComponent.prototype.userHasHeartedThis = function (sl) {
        var found = -1;
        if (this.dashboard.user == null)
            return -1;
        for (var i = 0; i < sl.hearts.length; i++) {
            if (sl.hearts[i].userId == this.dashboard.user.id) {
                return i;
            }
        }
        return -1;
    };
    DayStreamComponent.prototype.toggleSilverLiningHeart = function (sl) {
        if (this.dashboard.user.isAnonymous()) {
            alert("You have to be logged in to <3 :) ");
            return;
        }
        var found = this.userHasHeartedThis(sl);
        if (found > -1) {
            this.unheartSilverLining(sl, found);
        }
        else {
            this.heartSilverLining(sl);
        }
    };
    DayStreamComponent.prototype.heartSilverLining = function (sl) {
        // add authorization header with jwt token
        var headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
        var options = new RequestOptions({ headers: headers });
        this.http.post(HOST + "/silver-lining-hearts/" + sl.id + "/" + this.dashboard.user.id, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log("heartSilverLining", data);
            sl.hearts.push(new SilverLiningHeart(data));
        });
    };
    DayStreamComponent.prototype.unheartSilverLining = function (sl, heartIndex) {
        // add authorization header with jwt token
        var headers = new Headers({ 'Authorization': 'Bearer ' + this.auth.getToken() });
        var options = new RequestOptions({ headers: headers });
        console.log(HOST + "/silver-lining-hearts/" + sl.id + "/" + this.dashboard.user.id + "/delete", options);
        this.http.post(HOST + "/silver-lining-hearts/" + sl.id + "/" + this.dashboard.user.id + "/delete")
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            sl.hearts.splice(heartIndex, 1);
        });
    };
    DayStreamComponent.prototype.medium = function () {
        this.http.get(HOST + "/medium/authorize")
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log("RESPONSE: ", data);
        });
    };
    DayStreamComponent.prototype.uploadFile = function () {
        var _this = this;
        // this.fileUpload = $event.target.files[0];
        console.log(this.fileUpload);
        if (this.fileUpload) {
            this.inProgress = true;
            var headers = new Headers();
            var formData = new FormData();
            formData.append('files', this.fileUpload, this.fileUpload.name);
            this.http.post(HOST + "/uploads", formData, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log("RESPONSE: ", data);
                if (_this.fileUpload.name.indexOf('.txt') > 1 || _this.fileUpload.name.indexOf('.pdf') > 1) {
                    _this.newSilverLining.description = '<a href="' + HOST + data + '">' + _this.fileUpload.name + '</a>';
                }
                else {
                    _this.newSilverLining.description = '<img src="' + HOST + data + '" class="card-content"/>';
                }
                _this.addSilverLining();
                _this.uploadAFile = !_this.uploadAFile;
                _this.fileUpload = null;
                _this.inProgress = false;
            });
        }
        else {
            alert("Please choose a file!");
        }
    };
    DayStreamComponent.prototype.fileInputChanged = function ($event) {
        this.fileUpload = $event.target.files[0];
        this.newSilverLining.description = $event.target.files[0].name;
        this.isAFileSl = true;
    };
    DayStreamComponent.prototype.post = function () {
        if (this.uploadAFile) {
            this.uploadFile();
        }
        else {
            this.addSilverLining();
        }
    };
    DayStreamComponent.prototype.togglePublic = function (sl) {
        if (sl.user && sl.user.username == "anonymous") {
            alert("Please, login / register in order to be able to make something private :)! ");
            return;
        }
        sl.isPublic = !sl.isPublic;
    };
    DayStreamComponent.prototype.showDetails = function (sl) {
        this.hideAllTooltips();
        this.detailsOpen = true;
        this.activeSilverLining = sl;
        this.mediumEditor = new MediumEditor('.editable');
    };
    DayStreamComponent.prototype.ngOnDestroy = function () {
        // unsubscribe to ensure no memory leaks
        this.authSubscription.unsubscribe();
    };
    DayStreamComponent.prototype.toggleUpload = function () {
        this.hideAllTooltips();
        this.uploadAFile = !this.uploadAFile;
        if (this.uploadAFile) {
            this.initAllTooltips();
        }
    };
    return DayStreamComponent;
}());
DayStreamComponent = __decorate([
    Component({
        selector: 'main-container',
        templateUrl: '../templates/day-stream.component.html',
        styleUrls: ["../css/day-stream.component.css"],
        providers: [HttpModule]
    }),
    Injectable(),
    __param(0, Inject(Http)),
    __metadata("design:paramtypes", [Object, Auth,
        InProgressSvc,
        DaySvc,
        UserSvc,
        ActivatedRoute])
], DayStreamComponent);
export { DayStreamComponent };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/components/day-stream.component.js.map