var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Auth } from '../services/auth.service';
import { User } from "../models";
var TopComponent = (function () {
    function TopComponent(auth) {
        this.auth = auth;
        this.loginOpen = false;
        this.getCurrentUser();
    }
    TopComponent.prototype.getCurrentUser = function () {
        if (this.auth.authenticated()) {
            this.currentUser = new User(JSON.parse(localStorage.getItem("usr")));
        }
    };
    TopComponent.prototype.onLoginOpenChange = function (loginOpen) {
        this.loginOpen = loginOpen;
        // console.log("this.loginOpen", this.loginOpen, this.auth.currentUser)
        // this.currentUser = this.auth.currentUser;
        this.getCurrentUser();
    };
    return TopComponent;
}());
TopComponent = __decorate([
    Component({
        selector: 'top',
        templateUrl: "../templates/top.component.html",
        styleUrls: ['../css/top.component.css']
    }),
    __metadata("design:paramtypes", [Auth])
], TopComponent);
export { TopComponent };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/components/top.component.js.map