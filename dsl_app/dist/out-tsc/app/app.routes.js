import { RouterModule } from '@angular/router';
import { DayStreamComponent } from './components/day-stream.component';
var appRoutes = [
    { path: '', component: DayStreamComponent },
    { path: '**', redirectTo: '' }
];
export var appRoutingProviders = [];
export var routing = RouterModule.forRoot(appRoutes);
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/app.routes.js.map