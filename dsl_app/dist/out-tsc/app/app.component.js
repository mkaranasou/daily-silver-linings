var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { Http } from "@angular/http";
//import {Auth} from './services/auth.service'
var AppComponent = (function () {
    function AppComponent(http) {
        this.http = http;
    }
    AppComponent.prototype.ngAfterViewInit = function () {
        // source: https://jmperezperez.com/medium-image-progressive-loading-placeholder/
        var placeholder = document.querySelector('.placeholder');
        var small = placeholder.querySelector('.calm');
        // 1: load small image and show it
        var img = new Image();
        img.src = small.src;
        img.onload = function () {
            small.classList.add('loaded');
        };
        // 2: load large image
        var imgLarge = new Image();
        imgLarge.src = placeholder.dataset.large;
        imgLarge.onload = function () {
            imgLarge.classList.add('loaded');
        };
        placeholder.appendChild(imgLarge);
        placeholder.removeChild(small);
        imgLarge.className = "calm loaded";
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Component({
        selector: 'app',
        templateUrl: "./templates/main.html",
        styleUrls: ['./app.component.css'],
    }),
    __param(0, Inject(Http)),
    __metadata("design:paramtypes", [Object])
], AppComponent);
export { AppComponent };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/app.component.js.map