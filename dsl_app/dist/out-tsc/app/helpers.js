var Helpers = (function () {
    function Helpers() {
    }
    Helpers.convertToCamelCase = function (dictData) {
        var converted = {};
        for (var key in dictData) {
            converted[key.replace(/(\_\w)/g, function (g) { return g[1].toUpperCase(); })] = dictData[key];
            /* if(dictData.hasOwnProperty(key)){ // not necessary since we're checking for json
             }*/
        }
        return converted;
    };
    return Helpers;
}());
export { Helpers };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/helpers.js.map