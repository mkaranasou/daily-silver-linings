import { Helpers } from "./helpers";
export var ActionType;
(function (ActionType) {
    ActionType[ActionType["none"] = 0] = "none";
    ActionType[ActionType["itemComplete"] = 1] = "itemComplete";
    ActionType[ActionType["itemAdded"] = 2] = "itemAdded";
    ActionType[ActionType["itemRemoved"] = 3] = "itemRemoved";
    ActionType[ActionType["itemFavorited"] = 4] = "itemFavorited";
    ActionType[ActionType["dayAdded"] = 5] = "dayAdded";
    ActionType[ActionType["dayRemoved"] = 6] = "dayRemoved";
    ActionType[ActionType["dayUpdated"] = 7] = "dayUpdated";
    ActionType[ActionType["dayFavorited"] = 8] = "dayFavorited";
    ActionType[ActionType["login"] = 9] = "login";
    ActionType[ActionType["logout"] = 10] = "logout";
    ActionType[ActionType["register"] = 11] = "register";
    ActionType[ActionType["resetPassword"] = 12] = "resetPassword";
})(ActionType || (ActionType = {}));
var Action = (function () {
    function Action(options) {
        options = options || {};
        this.id = options.id || null;
        this.type = options.type || ActionType.none;
        this.actor = options.actor || null;
        this.item = options.item || null;
        this.message = this.getMessage();
    }
    Action.prototype.getMessage = function () {
        switch (this.type) {
            case ActionType.none:
                return "";
            case ActionType.itemComplete:
                return this.actor.username + " completed " + this.item.description + "!!!";
            case ActionType.itemAdded:
                return this.actor.username + " added '" + this.item.description + "'!";
            case ActionType.itemRemoved:
                return this.actor.username + " removed '" + this.item.description + "'.";
            case ActionType.itemFavorited:
                return this.actor.username + " liked '" + this.item.description + "'!";
            case ActionType.dayAdded:
                return this.actor.username + " added " + this.item.dateStr + " day!";
            case ActionType.dayRemoved:
                return this.actor.username + " removed " + this.item.dateStr + " day.";
            case ActionType.dayUpdated:
                return this.actor.username + " updated " + this.item.dateStr + " day!!!";
            case ActionType.dayFavorited:
                return this.actor.username + " liked " + this.item.dateStr + " day!";
            default:
                return "";
        }
    };
    return Action;
}());
export { Action };
var Attachment = (function () {
    function Attachment(options) {
        options = options || {};
        this.description = options.name || "";
        this.filePath = options.filePath || "";
        var temp = this.filePath.split(".");
        this.fileType = options.fileType || temp[temp.length - 1];
        this.parent = options.parent;
    }
    Attachment.prototype.asJSON = function () {
        return {
            "description": this.description,
            "filePath": this.filePath,
            "fileType": this.fileType,
        };
    };
    Attachment.prototype.toEditor = function () {
        switch (this.fileType) {
            case "tiff":
            case "jpeg":
            case "png":
            case "jpg":
                return "<img src='" + this.filePath + "' class='card-content'/>";
            case "txt":
            case "pdf":
                return "<a href='" + this.filePath + "'>" + this.description + "</a>";
        }
    };
    return Attachment;
}());
export { Attachment };
var SilverLiningHeart = (function () {
    function SilverLiningHeart(options) {
        options = options || {};
        options = Helpers.convertToCamelCase(options);
        this.id = options.id || null;
        this.itemId = options.itemId || null;
        this.userId = options.userId || null;
    }
    return SilverLiningHeart;
}());
export { SilverLiningHeart };
var SilverLining = (function () {
    function SilverLining(options) {
        options = options || {};
        options = Helpers.convertToCamelCase(options);
        this.id = options.id || null;
        this.description = options.description;
        this.user = options.user || new User({ username: "anonymous" });
        this.isApproved = options.isApproved === true;
        this.isPublic = options.isPublic === true;
        this.allowedToSee = options.allowedToSee === true;
        this.hearts = options.hearts ? this.loadHearts(options.hearts) : [];
        this.attachment = this.loadAttachment(options.attachment);
        this.description = this.toEditor();
    }
    SilverLining.prototype.asJSON = function () {
        return {
            "id": this.id,
            "description": this.description,
            "isApproved": this.isApproved,
            "isPublic": this.isPublic,
            "userId": this.user.id,
            "attachment": this.attachment ? this.attachment.asJSON() : null
        };
    };
    SilverLining.prototype.toEditor = function () {
        return this.attachment ? this.attachment.toEditor() : this.description;
    };
    SilverLining.prototype.loadAttachment = function (attachment) {
        if (!attachment)
            return null;
        return new Attachment(attachment);
    };
    SilverLining.prototype.loadHearts = function (hearts) {
        var hs = [];
        for (var i = 0; i < hearts.length; i++) {
            var heart = Helpers.convertToCamelCase(hearts[i]);
            hs.push(new SilverLiningHeart(heart));
        }
        return hs;
    };
    return SilverLining;
}());
export { SilverLining };
var OneGoodDay = (function () {
    function OneGoodDay(options) {
        options = options || {};
        options = Helpers.convertToCamelCase(options);
        this.id = options.id || null;
        // if we have a date then it is from the server - thus in utc - convert it back to local
        this.date = options.date ? OneGoodDay.getLocalDate(options.date) : new Date();
        this.dateStr = options.dateStr || OneGoodDay.getStrDate(this.date);
        this.silverLinings = options.silverLinings ? this.loadSilverLinings(options.silverLinings) : [];
    }
    OneGoodDay.getLocalDate = function (date) {
        date = new Date(date);
        date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
        return date;
    };
    OneGoodDay.getStrUTCToLocalDate = function (date) {
        date = new Date(date);
        date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
        return date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();
    };
    OneGoodDay.getStrUTCDate = function (date) {
        date = date || new Date();
        return date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();
    };
    OneGoodDay.getStrDate = function (date) {
        date = date || new Date();
        return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    };
    OneGoodDay.prototype.loadSilverLinings = function (silverLinings) {
        var sls = [];
        for (var i = 0; i < silverLinings.length; i++) {
            var sl = Helpers.convertToCamelCase(silverLinings[i]);
            sls.push(new SilverLining(sl));
        }
        return sls;
    };
    return OneGoodDay;
}());
export { OneGoodDay };
var User = (function () {
    function User(options) {
        options = options || {};
        options = Helpers.convertToCamelCase(options);
        this.id = options.id;
        this.username = options.username;
        this.email = options.email;
        this.password = options.password;
        this.profile = options.profile || {};
        this.isValid = options.is_valid === true;
        this.updateOthers = options.updateOthers === true;
        this.silverLinings = options.silverLinings || [];
    }
    User.prototype.isAnonymous = function () {
        return this.username == "anonymous" && this.password == null;
    };
    return User;
}());
export { User };
var Dashboard = (function () {
    function Dashboard(options) {
        options = options || {};
        this.currentDate = options.currentDate || new OneGoodDay();
        this.user = options.user || new User({ name: "Anonymous", isValid: true });
    }
    return Dashboard;
}());
export { Dashboard };
var CommonMessage = (function () {
    function CommonMessage(options) {
        options = options || {};
        this.success = options.success || false;
        this.message = options.message || "";
        this.data = options.data || null;
        this.action = options.action || ActionType.none;
    }
    return CommonMessage;
}());
export { CommonMessage };
var BaseSvc = (function () {
    function BaseSvc() {
    }
    BaseSvc.prototype.setInprogress = function () {
        this.inProgress = true;
    };
    BaseSvc.prototype.done = function () {
        this.inProgress = false;
    };
    BaseSvc.prototype.isInProgress = function () {
        return this.inProgress;
    };
    return BaseSvc;
}());
export { BaseSvc };
//# sourceMappingURL=/home/mkaranasou/Dev/mk/daily-silver-linings/dsl_app/front-end/src/app/models.js.map