# README #
### What is this repository for? ###

**29/3/17**:

This is a simple project with a simple goal: to document the daily small good things that happen so that 
we keep perspective and get a boost to get on with our day.

The front-end is in Angular2 and uses AngularCLI, Bootstrap 4 and fontawesome.
The back-end is a simple rest API with Flask and Flask-SQLAlchemy. 

Everything is currently in a very early stage.

* Version 0.2.0 beta

**05/04/2017**:
Flask app deployed on http://mkaranasou.pythonanywhere.com

**10/4/2017**:
Angular2 deployed on http://www.silverlinings.today/ as a beta version.

### How do I get set up? ###

* **Summary of set up**
The following are neccessary:
    - Front-end: The latest Nodejs, Npm, TypeScript.
    - Back-end: Python 2.7.x, Flask, SQLAlchemy, Flask-cors, enum34 
    (the basics, use the requirements.txt that lists everything used)

* **Configuration**
    
    ##### For the Front-end:
    
    - Install the latest node:
 Ubuntu:
    ```commandline
    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    sudo apt-get install -y nodejs
    ```
         
    - `cd /path/to/daily-silver-linings/dsl_app/front-end` 
    - Install npm `apt install npm`
    - Install the latest Typescript
        `npm install -g typescript`
    - Install angular-cli 
      `npm install -g angular-cli`
    - After all is set and done, run `npm install` while in the `front-end` directory
    - Run `tsc` in the `src` directory, where `tsconfig.json` exists (and you can run in a separate console `tsc -w` to re-transpile on changes)
    - Run `npm start`
    If everything goes well, the site will be up and running in
    `http://localhost:4200`
    
    ##### For the back-end (API):
    - Preferably create a virtualenv and run everything through there, but that's optional.
    - `pip install -r /path/to/requirements.txt`
* **Dependencies:** See Setup summary.
* **Database configuration**

    ##### SQLite
    For the time being SQLite is used. Just change the path in ``, remember four slashes means absolute, three relative.
    Also, make sure you have the required permissions in the folder you choose.

    ##### Postgres or MySQL
    The project will migrate to something more robust in the future.
    
* **How to run tests**
   
    `npm test`  for the front-end


* **Deployment instructions:**

    Basically the front-end is cdn-like and the back-end a simple Flask API, that can be run and hosted separately.
    
    ##### Front-end:
    Run `ng build --prod` to generate the `dist` folder with the production accumulation of the app.
    This will be the folder to be set up and deployed on an **nginx**.
    - Install nginx
    - set the root of the default config of nginx to the /path/to/dist (`root=/path/to/dist;`)
    - restart nginx
    
    Note: in index.html, there is a `const HOST`. Set this to the backend url.
    
    ##### Backend:
    The easiest way - and probably the cheapest - is to deploy the Flask app 
    in pythonanywhere, with a free account. The one thing that needs to be changed is
    the path to `test.db` which needs to be something absolute where you deploy it and 
    the wsgi configuration to point to where your app is.
    

TODOs
---
- Proper url navigation for days: 
  
  From `silverlinings.today` to `silverlinings.today/year/month/day` or `silverlinings.today#YYYY-MM-DD`   
  
  This means use the routing component and change the way the day-stream.component loads.

- UI: 
   
        If anonymous:
        - ~~Do NOT see public-private option~~ cannot post if anonymous
        - ~~Do NOT see the ability to heart something - just how many hearts it has.~~ Cannot heart if anonymous
        - ~~Do NOT see the ability to edit - maybe just the first few seconds and then nada.~~ Cannot edit if anonymous
        - ~~Upload button should open a file-picker and not change the whole input. This should need some work. - WIP~~
    - Show my silver-linings for logged-in users.
    - Show my hearts for logged-in users.
    - Notify the user that someone did something - use sockets.
    
- Changes in the back-end:
    - Go from sqlite to MySQL for the time being and perhaps POSTGRES if necessary.
    - Would be nice to switch to Mongo - future work
    - ~~Secure the endpoints that need securing:~~
        - ~~Users [GET/POST/PUT/DELETE]/~~
        - ~~GoodDays [DELETE]/~~
        - ~~SilverLinings [DELETE]/~~
        - ~~Hearts [POST/PUT/DELETE]/~~
    - Bind the Attachments to Silverlinings and handle them properly - WIP
    - ~~Sort Silverlinings by datetime descending to serve latest first.~~
    - Email verification after registration - WIP
    - Swagger documentation - WIP
    - Unit and Entity testing - WIP
    - Add socket.io functionality to notify users: https://flask-socketio.readthedocs.io/en/latest/ 
    - Change the base url to '/api/v{version number}/'
    - ~~Add token based authentication~~
    - Check this: https://pythonhosted.org/Flask-Social/


### Who do I talk to? ###

* @mkaranasou